<?php
    session_start();
    
    require("database-admin.php");
    $koneksi = connect_database();
    $username = mysqli_real_escape_string($koneksi,$_POST["username"]);
    $password = mysqli_real_escape_string($koneksi,$_POST["password"]);
    $admin = get_admin($username);

    if(empty($username) || empty($password)){
        header("Location: admin.php?status=1");
    }
    
    else if($password == $admin['password']){
        $_SESSION["USERNAME"] = $username;
        $_SESSION["FIRST_NAME"] = $admin['first_name'];
        $_SESSION["LAST_NAME"] = $admin['last_name'];
        $_SESSION["EMAIL"] = $admin['email'];
        $_SESSION["STATUS"] = "login";
        session_regenerate_id(true);
        header("Location: admin-home.php");
    }   else{
        
        header("Location: admin.php?status=1");
    }
?>