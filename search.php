<!DOCTYPE html>
<?php
    session_start();
    $key = $_GET['key'];
    $type = $_GET['type'];
include "database.php";
    $jumlah = count(get_notification_not_read($_SESSION["NIM"]));
?>

<html>
    <head>
        <title>UKDW Creative - Search</title>
        <link type="text/css" rel="stylesheet" href="design.css">
        <script src="JavaScript.js"></script>
    </head>
    
    <body>
        <div id="containerheader">
            
            <a href="home.php"><img id="leftlogo" src="uc.png"/></a>
            <a href="plus.google.com"><img id="gplus" src="gplus-mono.png" onmouseover="mOverGoogle(this)" onmouseout="mOutGoogle(this)"/></a>
            <a href="twitter.com"><img id="twitter" src="twitter-mono.png" onmouseover="mOverTwitter(this)" onmouseout="mOutTwitter(this)"/></a>
            <a href="facebook.com"><img id="fb" src="fb-mono.png" onmouseover="mOverFacebook(this)" onmouseout="mOutFacebook(this)"/></a>
            <?php
            if(isset($_SESSION['NIM'])){
                echo '
                <div id="myNav" class="overlay">

  <!-- Button to close the overlay navigation -->
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">x</a>

  <!-- Overlay content -->
  <div class="overlay-content">';
                ?>
            <?php
            require_once("database.php");
            $notification_array;
        $notification_array = get_notification($_SESSION["NIM"]);
                rsort($notification_array);
        if(count($notification_array) > 0):
                
				foreach($notification_array as $row): ?>
				
					<?php
                
                    echo '<a href="post.php?id='.$row['post'].'"><img style="height:50px;width:80px;float:left;" src="'.$row['picture'].'">Mahasiswa dengan NIM '.$row['dari'].' telah '.$row['type'].' post anda!</a>';
                        
                    
                    
                    ?>
        
			<?php endforeach; 
				 ?>
				
			<?php endif;
        ?>
<?php
  echo '</div>

</div>
';
?>
<!-- Use any element to open/show the overlay navigation menu -->
            <?php
                if($jumlah > 0){
                    echo '<span id="notification_count" style="float:right">'.$jumlah.'</span>';
                }    
            ?>

            <?php echo'
<span onclick="openNav()" style="float:right;font-size:40px;margin:0px;">=</span>

                ';
            }
            ?>
            <table class="profile_bar">
                <tbody>
                    <tr>
                        <td rowspan="2"><img class="profile_picture" src="guest.png"/></td>
                        <?php
                            echo "<td>Welcome, ";
                            if(isset($_SESSION['NIM'])){
                                echo '<a href="profile.php?nim=';
                                echo $_SESSION['NIM'];
                                echo '">';
                                echo $_SESSION['FIRST_NAME'];
                                echo '</a>';
                                echo'
                                <tr>
                                    <td><a href="logout.php">Logout</a></td>
                                </tr>';
                            }
                            else{
                                echo "Guest";
                                 echo'
                                <tr>
                                    <td><a href="login.php">Login</a></td>
                                </tr>';
                            }
                                
                            echo "</td>";
                        ?>
                    </tr>
                   
                    
                </tbody>
            </table>
        </div>
        <div id="containernav">
            <ul>
                <li><a class="active" href="home.php">HOME</a></li>
                <li><div class="dropdown">
                    <a href="#" class="dropbutton">PORTFOLIO</a>
                    <div class="dropdown-content">
                        <a href="category.php?type=websites">WEBSITES</a>
                        <a href="category.php?type=applications">APPLICATIONS</a>
                        <a href="category.php?type=multimedia">MULTIMEDIA</a>
                    </div>
                </div>
                </li>
                <li><a href="about.php">ABOUT</a></li>
                <li class="right">
                    <form id="search_bar" method="GET" action="search.php">
                        <input type="search" id="input_text" name="key" placeholder="Search">
                        <select name = "type">
                            <option value="all">All</option>
                            <option value="users">Users</option>
                            <option value="posts">Posts</option>
                        </select>
                        <button><img class="search_logo" src="search-256.png"></button>
                    </form>
                </li>
            </ul>
        </div>
        <div id="containersection">
            
            <h2>Hasil Pencarian untuk "<?php echo $key?>"</h2>
            <br>
            
            <?php
            require_once("database.php");
            $post_array;
            if($type == "posts"){
                $post_array = search_post($key);
            }
            else if($type == "users"){
                $post_array = search_user($key);
            }
            rsort($post_array);
        if(count($post_array) > 0):
				foreach($post_array as $row): ?>
				
					<?php
            if($type == "posts"){
                echo '<h3>Posts</h3>';
                echo'
                        <div class="item2">
                <img class="item2_photo" src="'.$row['location'].'"/> 
                <div class="item2_description_layer">
                    <h3 class="item2_description">'.$row['title'].'</h3>;
                    <p class="item2_description">'.$row['desc'].'<a class="link" href="post.php?id='.$row['id'].'">See More..</a></p>
                </div> </div>
            
                    ';
            }
            else if($type == "users"){
                echo '<br/><h3>Users</h3>';
                echo '
                <div class="users_search">
                    <img class="profile_photo_size" src="'.$row['profile_picture'].'"/>
                    <p class="nomargin"><a href="profile.php?nim='.$row['nim'].'">'.$row['first_name'].' '.$row['last_name'].'</a></p>
                    </div>
                ';
            }
            else if($type == "all"){
                echo '<h3>Posts</h3>';
                echo'
                        <div class="item2">
                <img class="item2_photo" src="'.$row['location'].'"/> 
                <div class="item2_description_layer">
                    <h3 class="item2_description">'.$row['title'].'</h3>;
                    <p class="item2_description">'.$row['desc'].'<a class="link" href="post.php?id='.$row['id'].'">See More..</a></p>
                </div> </div>
            
                    ';
                echo '<br/><h3>Users</h3>';
                echo '
                <div class="profile_photo">
                    <img class="profile_photo_size" src="'.$row['profile_picture'].'"/>
                    <p class="nomargin"><a href="profile.php?nim='.$row['nim'].'">'.$row['first_name'].' '.$row['last_name'].'</a></p>
                    </div>
                ';
            }
                    
                    ?>
        
			<?php endforeach; 
				else: ?>
				<h3>No search available/found!</h3>
			<?php endif;
        ?>

        </div>
        
        <div id="containerfooter">
            <hr>
            <table>
              <tr>
                <td><p id="footerkiri"><i>&copy; 2016 | UKDW Creative</i></p></td>
                <td><p id="footerkanan"><i>Progweb Ceria's Team</i></p></td>
              </tr>
            </table>
        </div>
    </body>
</html>
