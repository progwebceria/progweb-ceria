<?php
    session_start();
	
	if(isset($_GET['id']) && isset($_SESSION["USERNAME"])) {
		$id = $_GET['id'];
		//ambil quote dengan id tersebut
		require_once("database-admin.php");
		$user = get_user($id);

		
		if(!count($user)) {
			header("Location: admin-home.php");
		}
	}
	else {
		header("Location: admin-home.php");
	}		
?>
<!DOCTYPE html>
<html>
<head>
	<title>Edit Profile <?php echo $user['first_name']?></title>
    <link type="text/css" rel="stylesheet" href="design.css">
</head>
<body>
	<div class="container">
		<header>Edit Profile <?php echo $user['first_name']?> (NIM : <?php echo $user['nim']; ?>)</header>
		<section>
			
			<form method="post" action="edituser.php">
                
				<input type="hidden" name="nim" value="<?php echo $user['nim']; ?>"><br>
				
                <label>Full Name:</label><br>
				<input type="text" name="fullname" value="<?php echo $user['first_name']; ?> <?php echo $user['last_name']; ?>" disabled><br>
                <label>Prodi:</label><br>
				<input type="text" name="prodi" value="<?php echo $user['prodi']; ?>" disabled><br>
                <label>Email:</label><br>
				<input type="text" name="email" value="<?php echo $user['email']; ?>"><br>
                <label>Google:</label><br>
                <label>https://plus.google.com/</label>
				<input type="text" name="google" value="<?php echo $user['google']; ?>"><br>
                <label>Twitter:</label><br>
                <label>https://twitter.com/</label>
				<input type="text" name="twitter" value="<?php echo $user['twitter']; ?>"><br>
                <label>Facebook:</label><br>
                <label>https://facebook.com/</label>
				<input type="text" name="facebook" value="<?php echo $user['facebook']; ?>"><br>
				<button type="submit">Update</button> | <a href="admin-home.php">Cancel</a>
			</form>
		</section>
	</div>
</body>
</html>