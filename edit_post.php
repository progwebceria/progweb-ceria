<!DOCTYPE html>

<?php
$id = $_GET['id'];
require("database.php");
$post = get_post($id);
session_start();
if(!isset($_SESSION["NIM"])){
    header("Location : login.php");
}

if(isset($_SESSION["NIM"]) && ($_SESSION["NIM"] != $post['nim'])){
    header("Location: login.php");
}



?>

<html>
    <head>
        <title>UKDW Creative - Edit Post</title>
        <link type="text/css" rel="stylesheet" href="design.css">
        <script src="JavaScript.js"></script>
    </head>
    
    <body>
        <div id="containerheader">
            <a href="home.php"><img id="leftlogo" src="uc.png"/></a>
            <a href="plus.google.com"><img id="gplus" src="gplus-mono.png" onmouseover="mOverGoogle(this)" onmouseout="mOutGoogle(this)"/></a>
            <a href="twitter.com"><img id="twitter" src="twitter-mono.png" onmouseover="mOverTwitter(this)" onmouseout="mOutTwitter(this)"/></a>
            <a href="facebook.com"><img id="fb" src="fb-mono.png" onmouseover="mOverFacebook(this)" onmouseout="mOutFacebook(this)"/></a>
            
            <table class="profile_bar">
                <tbody>
                    <tr>
                        <td rowspan="2"><img class="profile_picture" src="guest.png"/></td>
                        <?php 
                            echo "<td>Welcome, ";
                            if(isset($_SESSION['NIM'])){
                                echo '<a href="profile.php?nim=';
                                echo $_SESSION['NIM'];
                                echo '">';
                                echo $_SESSION['FIRST_NAME'];
                                echo '</a>';
                                echo'
                                <tr>
                                    <td><a href="logout.php">Logout</a></td>
                                </tr>';
                            }
                            else{
                                echo '<td>Welcome,Guest</td>';
                                echo '<tr>
                        <td><a href="login.php">Login</a></td>
                    </tr>';
                            }
                        ?> 
                    </tr>
                    
                    
                </tbody>
            </table>
        </div>
        <div id="containernav">
            <ul>
                <li><a href="home.php">HOME</a></li>
                <li><div class="dropdown">
                    <a href="#" class="dropbutton">PORTFOLIO</a>
                    <div class="dropdown-content">
                        <a href="websites.php">WEBSITES</a>
                        <a href="applications.php">APPLICATIONS</a>
                        <a href="multimedia.php">MULTIMEDIA</a>
                    </div>
                </div>
                </li>
                <li><a href="about.php">ABOUT</a></li>
                <li class="right">
                    <div id="search_bar">
                        <input type="search" id="input_text" name="googlesearch" placeholder="Search">
                        <button><img class="search_logo" src="search-256.png"></button>
                    </div>
                </li>
            </ul>
        </div>
        <div id="containersection">
            <form method="POST" action="edit_post_process.php" enctype="multipart/form-data">
                <input type="text" name="id" id="id" value="<?php echo $id;?>" hidden="hidden">
                <img class="profile_photo_size" src="<?php echo $post['location'];?>"/><br>
                Change your Post Picture :
                <br>
                <input type="file" name="upload" id="file">
                <br>
                Category: <br>
                <?php 
                if($post['type'] == "websites"){
                    
                echo '
                <input type="radio" name="type" value="websites" checked="checked"> Websites <br>
                <input type="radio" name="type" value="applications"> Applications <br>
                <input type="radio" name="type" value="multimedia"> Multimedia <br>
                ';
                }
                else if($post['type'] == "applications") {
                echo '
                <input type="radio" name="type" value="websites"> Websites <br>
                <input type="radio" name="type" value="applications" checked="checked"> Applications <br>
                <input type="radio" name="type" value="multimedia"> Multimedia <br>
                ';
                }
                else{
                   echo '
                <input type="radio" name="type" value="websites"> Websites <br>
                <input type="radio" name="type" value="applications" checked="checked"> Applications <br>
                <input type="radio" name="type" value="multimedia" checked="checked"> Multimedia <br>
                '; 
                }
                ?>
                <br>
                <label>Title:</label><br>
				<input type="text" name="title" value="<?php echo $post['title']; ?>"><br>
				
                <label>Description:</label><br>
                <textarea rows="5" cols="50 "name="description" id="description"><?php echo $post['desc']; ?></textarea><br>
				<input type="submit" name="Submit"> | <a href="profile.php?nim=<?php echo $_SESSION["NIM"];?>">Cancel</a>
			</form>
        </div>
        <div id="containerfooter">
            <hr>
            <table>
              <tr>
                <td><p id="footerkiri"><i>&copy; 2016 | UKDW Creative</i></p></td>
                <td><p id="footerkanan"><i>Progweb Ceria's Team</i></p></td>
              </tr>
            </table>
        </div>
    </body>
</html>