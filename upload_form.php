<?php
session_start();
if(!isset($_SESSION["NIM"])){
    header("Location: login.php");
}
?>
<!DOCTYPE html>

<html>
    <head>
        <title>UKDW Creative - Upload</title>
        <link type="text/css" rel="stylesheet" href="design.css">
    </head>
    
    <body>
        <div id="containerheader">
            
            <a href="home.php"><img id="leftlogo" src="uc.png"/></a>
            <a href="plus.google.com"><img id="gplus" src="gplus-warna.png"/></a>
            <a href="twitter.com"><img id="twitter" src="twitter-warna.png"/></a>
            <a href="facebook.com"><img id="fb" src="fb-warna.png"/></a>
            
            <table class="profile_bar">
                <tbody>
                    <tr>
                        <td rowspan="2"><img class="profile_picture" src="guest.png"/></td>
                        <?php
                            echo "<td>Welcome, ";
                            if(isset($_SESSION['NIM'])){
                                echo $_SESSION['FIRST_NAME'];
                                echo'
                                <tr>
                                    <td><a href="logout.php">Logout</a></td>
                                </tr>';
                            }   
                            echo "</td>";
                        ?>
                    </tr>
                    
                </tbody>
            </table>
        </div>
        <div id="containernav">
            <ul>
                <li><a href="home.php">HOME</a></li>
                <li><div class="dropdown">
                    <a class="active" href="#" class="dropbutton">PORTFOLIO</a>
                    <div class="dropdown-content">
                        <a href="websites.php">WEBSITES</a>
                        <a href="applications.php">APPLICATIONS</a>
                        <a href="multimedia.php">MULTIMEDIA</a>
                    </div>
                </div>
                </li>
                <li><a href="logged_about.php">ABOUT</a></li>
                
            </ul>
        </div>
        <div id="containersection">
            
            <div class="upload_section">
            <h1>Upload Form</h1>
            <form method="POST" action="upload_process.php" enctype="multipart/form-data">
                Category: <br>
                <input type="radio" name="type" value="websites"> Websites <br>
                <input type="radio" name="type" value="applications"> Applications <br>
                <input type="radio" name="type" value="multimedia"> Multimedia <br>
                <br>
                
                Title : <br><input type="text" name="title" id="title">
                <br>
                Description : <br />
                <textarea rows="5" cols="50" name="description" id="description">
                
                </textarea>
                <br>
                
                Choose a file :
                <br>
                <input type="file" name="upload" id="file">
                <br>
                
                <input type="submit" name="Submit" class="upload_form_button">
                <a href="home.php">Cancel</a>
            </form> 
                <br><br><br><br><br><br><br><br><br><br><br><br>
            </div>
            
            
            <div class="simple_way">
                <h1>Cuma 3 Langkah Mudah !</h1>
                <div class="ways">
                    <img class="ways_photo" src="4cbKLKxdi.png">
                    <h1>Isi Form</h1>
                    <p>Isikan judul dan deskripsi sesuai dengan keinginan anda ! Biarkan imajinasimu mengalir saat mengisinya!</p>
                </div>
                <div>
                <br><img class="dot" src="dot.png">
                <img class="dot" src="dot.png">
                <img class="dot" src="dot.png">
                </div>
                <div class="ways">
                    <img class="ways_photo" src="Docs-icon.png">
                    <h1>Pilih File yang Diinginkan</h1>
                    <p>Pilih file anda yang ingin ditunjukkan. Ingat,orang lain juga memiliki hasil yang terbaik,jadi pastikan hasil anda juga !</p>
                </div>
                <div>
                <br><img class="dot" src="dot.png">
                <img class="dot" src="dot.png">
                <img class="dot" src="dot.png">
                </div>
                <div class="ways">
                    <img class="ways_photo" src="Check_mark.png">
                    <h1>Submit</h1>
                    <p>Segera kirimkan dan kami akan langsung memasukkannya ke website kami,segera !</p>
                </div>
            </div>
        </div>
        <div id="containerfooter">
            <hr>
            <table>
              <tr>
                <td><p id="footerkiri"><i>&copy; 2016 | UKDW Creative</i></p></td>
                <td><p id="footerkanan"><i>Progweb Ceria's Team</i></p></td>
              </tr>
            </table>
        </div>
    </body>
</html>