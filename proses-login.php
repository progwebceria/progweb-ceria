<?php
    session_start();
    
    require("database.php");
    $koneksi = connect_database();
$nim = mysqli_real_escape_string($koneksi,$_POST["nim"]);
    $password = mysqli_real_escape_string($koneksi,$_POST["password"]);
    $user = get_user($nim);
    
    if(empty($nim) || empty($password)){
        header("Location: login.php?status=1");
    }
    else if($password == $user['password']){
        $_SESSION["NIM"] = $nim;
        $_SESSION["FIRST_NAME"] = $user['first_name'];
        $_SESSION["LAST_NAME"] = $user['last_name'];
        $_SESSION["PRODI"] = $user['prodi'];
        $_SESSION["EMAIL"] = $user['email'];
        $_SESSION["GOOGLE"] = $user['google'];
        $_SESSION["TWITTER"] = $user['twitter'];
        $_SESSION["FACEBOOK"] = $user['facebook'];
        $_SESSION["PROFILE_PICTURE"] = $user['profile_picture'];
        $_SESSION["STATUS"] = "login";
        session_regenerate_id(true);
        header("Location: home.php");
    }   else{
        header("Location: login.php?status=1");
    }
?>