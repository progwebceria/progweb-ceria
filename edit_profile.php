<!DOCTYPE html>

<?php
    session_start();
    if(!isset($_SESSION["NIM"])){
        header("Location : login.php");
    }
require("database.php");
$user = get_user($_SESSION["NIM"]);
?>

<html>
    <head>
        <title>UKDW Creative - Edit Profile</title>
        <link type="text/css" rel="stylesheet" href="design.css">
        <script src="JavaScript.js"></script>
    </head>
    
    <body>
        <div id="containerheader">
            <a href="home.php"><img id="leftlogo" src="uc.png"/></a>
            <a href="plus.google.com"><img id="gplus" src="gplus-mono.png" onmouseover="mOverGoogle(this)" onmouseout="mOutGoogle(this)"/></a>
            <a href="twitter.com"><img id="twitter" src="twitter-mono.png" onmouseover="mOverTwitter(this)" onmouseout="mOutTwitter(this)"/></a>
            <a href="facebook.com"><img id="fb" src="fb-mono.png" onmouseover="mOverFacebook(this)" onmouseout="mOutFacebook(this)"/></a>
            
            <table class="profile_bar">
                <tbody>
                    <tr>
                        <td rowspan="2"><img class="profile_picture" src="guest.png"/></td>
                        <?php 
                            echo "<td>Welcome, ";
                            if(isset($_SESSION['NIM'])){
                                echo '<a href="profile.php?nim=';
                                echo $_SESSION['NIM'];
                                echo '">';
                                echo $_SESSION['FIRST_NAME'];
                                echo '</a>';
                                echo'
                                <tr>
                                    <td><a href="logout.php">Logout</a></td>
                                </tr>';
                            }
                            else{
                                echo '<td>Welcome,Guest</td>';
                                echo '<tr>
                        <td><a href="login.php">Login</a></td>
                    </tr>';
                            }
                        ?> 
                    </tr>
                    
                    
                </tbody>
            </table>
        </div>
        <div id="containernav">
            <ul>
                <li><a href="home.php">HOME</a></li>
                <li><div class="dropdown">
                    <a href="#" class="dropbutton">PORTFOLIO</a>
                    <div class="dropdown-content">
                        <a href="websites.php">WEBSITES</a>
                        <a href="applications.php">APPLICATIONS</a>
                        <a href="multimedia.php">MULTIMEDIA</a>
                    </div>
                </div>
                </li>
                <li><a href="about.php">ABOUT</a></li>
                <li class="right">
                    <div id="search_bar">
                        <input type="search" id="input_text" name="googlesearch" placeholder="Search">
                        <button><img class="search_logo" src="search-256.png"></button>
                    </div>
                </li>
            </ul>
        </div>
        <div id="containersection">
            <form method="POST" action="edit_profile_process.php" enctype="multipart/form-data">
                <img class="profile_photo_size" src="<?php echo $user['profile_picture'];?>"/><br>
                Change your Profile Picture :
                <br>
                <input type="file" name="upload" id="file">
                <br>
                <label>NIM:</label><br>
				<input type="text" name="nim" value="<?php echo $_SESSION["NIM"]; ?>" disabled><br>
				
                <label>Full Name:</label><br>
				<input type="text" name="fullname" value="<?php echo $_SESSION["FIRST_NAME"]; ?> <?php echo $_SESSION["LAST_NAME"]; ?>" disabled><br>
                <label>Prodi:</label><br>
				<input type="text" name="prodi" value="<?php echo $_SESSION["PRODI"]; ?>" disabled><br>
                <label>Email:</label><br>
				<input type="text" name="email" value="<?php echo $user['email']; ?>"><br>
                <label>Google:</label><br>
                <label>https://plus.google.com/</label>
				<input type="text" name="google" value="<?php echo $user['google']; ?>"><br>
                <label>Twitter:</label><br>
                <label>https://twitter.com/</label>
				<input type="text" name="twitter" value="<?php echo $user['twitter']; ?>"><br>
                <label>Facebook:</label><br>
                <label>https://facebook.com/</label>
				<input type="text" name="facebook" value="<?php echo $user['facebook']; ?>"><br>
				<input type="submit" name="Submit"> | <a href="profile.php?nim=<?php echo $_SESSION["NIM"];?>">Cancel</a>
			</form>
        </div>
        <div id="containerfooter">
            <hr>
            <table>
              <tr>
                <td><p id="footerkiri"><i>&copy; 2016 | UKDW Creative</i></p></td>
                <td><p id="footerkanan"><i>Progweb Ceria's Team</i></p></td>
              </tr>
            </table>
        </div>
    </body>
</html>