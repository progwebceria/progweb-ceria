<?php
function connect_database() {
	//konfigurasi database
	$db_user = "user";
	$db_password = "GJAPLn35ZTbam3qw";
	$db_name = "user";
	$db_host = "localhost";
	
	//akan selalu dilakukan, lebih baik jika ditulis dalam sebuah fungsi tersendiri
	$koneksi = mysqli_connect($db_host, $db_user, $db_password, $db_name);
	return $koneksi;
}

function get_user($nim) {
	$koneksi = connect_database();
	$sql = "SELECT password,first_name,last_name,prodi,email,google,twitter,facebook,about,profile_picture,viewed,liked,commended FROM users WHERE nim = ?";
	$stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "s", $nim);
	mysqli_stmt_execute($stmt);
	mysqli_stmt_store_result($stmt);
	$user = array();
	if(mysqli_stmt_num_rows($stmt)) {
		mysqli_stmt_bind_result($stmt, $password , $first_name, $last_name, $prodi, $email,$google,$twitter,$facebook,$about,$profile_picture,$viewed,$liked,$commended);
		mysqli_stmt_fetch($stmt);

		//ubah ke bentuk array
		$user = array("nim" => $nim, "password"=> $password, "first_name" => $first_name, "last_name" => $last_name, "prodi" => $prodi, "email" => $email,"google" => $google,"twitter" => $twitter,"facebook" => $facebook,"about" => $about,"profile_picture" => $profile_picture,"viewed" => $viewed,"liked" => $liked,"commended" => $commended);
	}
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
	return $user;
}

function get_like($nim,$post) {
	$koneksi = connect_database();
	$sql = "SELECT * FROM likes WHERE likers = ? AND post = ?";
	$stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "si", $nim,$post);
	mysqli_stmt_execute($stmt);
	mysqli_stmt_store_result($stmt);
	$user = array();
	if(mysqli_stmt_num_rows($stmt)) {
		mysqli_stmt_bind_result($stmt, $id, $likers,$post);
		mysqli_stmt_fetch($stmt);

		//ubah ke bentuk array
		$user = array("id" => $id, "likers"=> $likers, "post" => $post);
	}
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
	return $user;
}

function get_user_posts($nim) {
	$koneksi = connect_database();
	$sql = "SELECT * FROM posts WHERE nim = ? AND status = 1";
    $stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "s", $nim);
    mysqli_stmt_execute($stmt);
	mysqli_stmt_bind_result($stmt,$id,$title,$nim,$type,$desc,$location,$status,$viewed,$liked,$commended);
	$post_array = array();
	while (mysqli_stmt_fetch($stmt)) {
		$single_quote = array("id" => $id, "title" => $title, "nim" => $nim, "type" => $type, "desc" => $desc, "location" => $location , "status" => $status,"viewed" => $viewed,"liked" => $liked,"commended" => $commended);
		$post_array[] = $single_quote;
	}
    mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
	return $post_array;	
}

function get_notification($nim) {
	$koneksi = connect_database();
	$sql = "SELECT * FROM `notifications` WHERE `ke` = ?";
    $stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "s", $nim);
    mysqli_stmt_execute($stmt);
	mysqli_stmt_bind_result($stmt,$id,$dari,$ke,$post,$type,$picture,$status);
	$post_array = array();
	while (mysqli_stmt_fetch($stmt)) {
		$single_quote = array("id" => $id, "dari" => $dari, "ke" => $ke, "post" => $post,"type" => $type,"picture" => $picture,"status" => $status);
		$post_array[] = $single_quote;
	}
    mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
	return $post_array;	
}

function get_notification_not_read($nim) {
	$koneksi = connect_database();
	$sql = "SELECT * FROM `notifications` WHERE `ke` = ? AND status = 0";
    $stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "s", $nim);
    mysqli_stmt_execute($stmt);
	mysqli_stmt_bind_result($stmt,$id,$dari,$ke,$post,$type,$picture,$status);
	$post_array = array();
	while (mysqli_stmt_fetch($stmt)) {
		$single_quote = array("id" => $id, "dari" => $dari, "ke" => $ke, "post" => $post,"type" => $type,"picture" => $picture,"status" => $status);
		$post_array[] = $single_quote;
	}
    mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
	return $post_array;	
}

function get_category_posts($type) {
	$koneksi = connect_database();
	$sql = "SELECT * FROM posts WHERE type = ? AND status = 1 ORDER BY id desc";
    $stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "s", $type);
    mysqli_stmt_execute($stmt);
	mysqli_stmt_bind_result($stmt,$id,$title,$nim,$type,$desc,$location,$status,$viewed,$liked,$commended);
	$post_array = array();
	while (mysqli_stmt_fetch($stmt)) {
		$single_quote = array("id" => $id, "title" => $title, "nim" => $nim, "type" => $type, "desc" => $desc, "location" => $location,"status" => $status,"viewed" => $viewed,"liked" => $liked,"commended" => $commended);
		$post_array[] = $single_quote;
	}
    mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
	return $post_array;	
}

function get_category_posts_comment($type) {
	$koneksi = connect_database();
	$sql = "SELECT * FROM posts WHERE type = ? AND status = 1 ORDER BY commended desc";
    $stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "s", $type);
    mysqli_stmt_execute($stmt);
	mysqli_stmt_bind_result($stmt,$id,$title,$nim,$type,$desc,$location,$status,$viewed,$liked,$commended);
	$post_array = array();
	while (mysqli_stmt_fetch($stmt)) {
		$single_quote = array("id" => $id, "title" => $title, "nim" => $nim, "type" => $type, "desc" => $desc, "location" => $location,"status" => $status,"viewed" => $viewed,"liked" => $liked,"commended" => $commended);
		$post_array[] = $single_quote;
	}
    mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
	return $post_array;	
}

function get_category_posts_favorite($type) {
	$koneksi = connect_database();
	$sql = "SELECT * FROM posts WHERE type = ? AND status = 1 ORDER BY liked desc";
    $stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "s", $type);
    mysqli_stmt_execute($stmt);
	mysqli_stmt_bind_result($stmt,$id,$title,$nim,$type,$desc,$location,$status,$viewed,$liked,$commended);
	$post_array = array();
	while (mysqli_stmt_fetch($stmt)) {
		$single_quote = array("id" => $id, "title" => $title, "nim" => $nim, "type" => $type, "desc" => $desc, "location" => $location,"status" => $status,"viewed" => $viewed,"liked" => $liked,"commended" => $commended);
		$post_array[] = $single_quote;
	}
    mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
	return $post_array;	
}

function get_all_posts() {
	$koneksi = connect_database();
	$sql = "SELECT * FROM posts WHERE status=1 ORDER by id desc";
	$result = mysqli_query($koneksi, $sql);

	//ubah ke bentuk array
	$post_array = array();
	while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
		$single_quote = array("id" => $row['id'], "title" => $row['title'], "nim" => $row['nim'], "type" => $row['type'], "desc" => $row['desc'], "location" => $row['location']);
		$post_array[] = $single_quote;
	}

	mysqli_close($koneksi);
	return $post_array;	
}

function get_all_posts_comment() {
	$koneksi = connect_database();
	$sql = "SELECT * FROM posts WHERE status=1 ORDER by commended desc";
	$result = mysqli_query($koneksi, $sql);

	//ubah ke bentuk array
	$post_array = array();
	while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
		$single_quote = array("id" => $row['id'], "title" => $row['title'], "nim" => $row['nim'], "type" => $row['type'], "desc" => $row['desc'], "location" => $row['location']);
		$post_array[] = $single_quote;
	}

	mysqli_close($koneksi);
	return $post_array;	
}

function get_all_posts_favorite() {
	$koneksi = connect_database();
	$sql = "SELECT * FROM posts WHERE status=1 ORDER by liked desc";
	$result = mysqli_query($koneksi, $sql);

	//ubah ke bentuk array
	$post_array = array();
	while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
		$single_quote = array("id" => $row['id'], "title" => $row['title'], "nim" => $row['nim'], "type" => $row['type'], "desc" => $row['desc'], "location" => $row['location']);
		$post_array[] = $single_quote;
	}

	mysqli_close($koneksi);
	return $post_array;	
}




function add_posts($title,$nim,$type,$desc,$location) {
	$koneksi = connect_database();

	//escape input
	$title = mysqli_real_escape_string($koneksi, $title);
	$nim = mysqli_real_escape_string($koneksi, $nim);
    $type = mysqli_real_escape_string($koneksi, $type);
    $desc = mysqli_real_escape_string($koneksi, $desc);
    $location = mysqli_real_escape_string($koneksi, $location);

	$sql = "INSERT INTO `posts` (`title`, `nim`, `type`, `desc`, `location`, `status`) VALUES (?, ?, ?, ?, ?, 0)";
	$stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "sssss", $title, $nim,$type,$desc,$location);
	mysqli_stmt_execute($stmt);
	$newid = mysqli_insert_id($koneksi);
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
	return $newid;
}

function get_post($id) {
	$koneksi = connect_database();
	$sql = "SELECT `id`,`title`,`nim`,`type`,`desc`,`location`,`viewed`,`liked`,`commended` FROM `posts` WHERE id = ?";
	$stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "i", $id);
	mysqli_stmt_execute($stmt);
	mysqli_stmt_store_result($stmt);
	$user = array();
	if(mysqli_stmt_num_rows($stmt)) {
		mysqli_stmt_bind_result($stmt, $id,$title, $nim, $type, $desc, $location,$viewed,$liked,$commended);
		mysqli_stmt_fetch($stmt);

		//ubah ke bentuk array
		$post = array("id" => $id, "title" => $title, "nim"=> $nim, "type" => $type, "desc" => $desc, "location" => $location,"viewed" => $viewed,"liked" => $liked,"commended" => $commended);
	}
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
	return $post;
}

function update_post($id,$title,$type,$desc,$location) {
	$koneksi = connect_database();

	//escape input
	$title = mysqli_real_escape_string($koneksi, $title);
    $type = mysqli_real_escape_string($koneksi, $type);
    $desc = mysqli_real_escape_string($koneksi, $desc);
    $location = mysqli_real_escape_string($koneksi, $location);

	$sql = "UPDATE `posts` SET `title` = ? , `type` = ? , `desc` = ? , `location` = ? WHERE `id` = ?";
	$stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "ssssi", $title,$type,$desc,$location,$id);
	mysqli_stmt_execute($stmt);
	$newid = mysqli_insert_id($koneksi);
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
	return $newid;
}

function update_profile($nim,$email,$google,$twitter,$facebook,$profile_picture) {
	$koneksi = connect_database();
	
	//escape input
	$email = mysqli_real_escape_string($koneksi, $email);

	$sql = "UPDATE users SET email = ? , google = ? , twitter = ? , facebook = ? , profile_picture = ? WHERE nim = ?";
	$stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "ssssss", $email, $google, $twitter, $facebook,$profile_picture, $nim);
	mysqli_stmt_execute($stmt);
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
}

function delete_post($id) {
	$koneksi = connect_database();
	$sql = "DELETE FROM posts WHERE id = ?";
	$stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "i", $id);
	mysqli_stmt_execute($stmt);
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
}

function search_post ($key) {
	$koneksi = connect_database();

	//escape input
	$key = "%".strtoupper(mysqli_real_escape_string($koneksi, $key))."%";

	$sql = "SELECT `id`, `title`, `nim`,`type`,`desc`,`location` FROM `posts` WHERE upper(`title`) like ? AND `status`=1";
	$stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "s", $key);
	mysqli_stmt_execute($stmt);
	mysqli_stmt_bind_result($stmt, $id, $title, $nim, $type, $desc, $location);
	$post_array = array();
	while(mysqli_stmt_fetch($stmt)) {
		$single_quote = array("id" => $id, "title" => $title, "nim" => $nim, "type" => $type, "desc" => $desc, "location" => $location);
		$post_array[] = $single_quote;
	}
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
	return $post_array;
}

function get_comments ($key) {
	$koneksi = connect_database();

	//escape input
	

	$sql = "SELECT `id`, `commentator`, `post`,`comment`,`profile_picture`,`date_comment` FROM `comments` WHERE `post` = ?";
	$stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "i", $key);
	mysqli_stmt_execute($stmt);
	mysqli_stmt_bind_result($stmt, $id, $commentator, $post, $comment, $profile_picture,$date_comment);
	$post_array = array();
	while(mysqli_stmt_fetch($stmt)) {
		$single_quote = array("id" => $id, "commentator" => $commentator, "post" => $post, "comment" => $comment, "profile_picture" => $profile_picture,"date_comment" => $date_comment);
		$post_array[] = $single_quote;
	}
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
	return $post_array;
}

function search_user ($key) {
	$koneksi = connect_database();

	//escape input
	$key = "%".strtoupper(mysqli_real_escape_string($koneksi, $key))."%";

	$sql = "SELECT `nim`, `first_name`, `last_name`,`prodi`,`email`,`google`,`twitter`,`facebook`,`profile_picture` FROM `users` WHERE (upper(`first_name`) like ?) OR (upper(`last_name`) like ?) OR (`nim` like ?)";
	$stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "sss", $key, $key, $key);
	mysqli_stmt_execute($stmt);
	mysqli_stmt_bind_result($stmt, $nim,$first_name,$last_name,$prodi,$email,$google,$twitter,$facebook,$profile_picture);
	$post_array = array();
	while(mysqli_stmt_fetch($stmt)) {
		$single_quote = array("nim" => $nim, "first_name" => $first_name, "last_name" => $last_name, "prodi" => $prodi, "email" => $email, "google" => $google, "twitter" => $twitter, "facebook" => $facebook, "profile_picture" => $profile_picture);
		$post_array[] = $single_quote;
	}
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
	return $post_array;
}



?>