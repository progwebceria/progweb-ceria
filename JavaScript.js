function validateForm() {
    "use strict";
    var x = document.getElementById('nim').value;
    
    if (x === "71140050") {
        window.alert("Selamat datang " + x);
    }
}

function mOverGoogle(obj) {
    "use strict";
    obj.src = "gplus-warna.png";
}

function mOutGoogle(obj) {
    "use strict";
    obj.src = "gplus-mono.png";
}


function mOverTwitter(obj) {
    "use strict";
    obj.src = "twitter-warna.png";
}

function mOutTwitter(obj) {
    "use strict";
    obj.src = "twitter-mono.png";
}

function mOverFacebook(obj) {
    "use strict";
    obj.src = "fb-warna.png";
}

function mOutFacebook(obj) {
    "use strict";
    obj.src = "fb-mono.png";
}

function hoverSelected(obj) {
    "use strict";
    obj.src = "more--three-dots--ios-7-interface-symbol_318-35324.jpg";
}

function hover(obj) {
    "use strict";
    obj.src = "menu-dots--ios-7-interface-symbol_318-38790.jpg";
}

function openNav() {
    "use strict";
    var jaringan = new XMLHttpRequest();
    document.getElementById("myNav").style.width = "23%";
    document.getElementById("notification_count").style.display = "none";
    jaringan.open("POST", "read_notification.php", true);
    jaringan.send();
}

function closeNav() {
    "use strict";
    document.getElementById("myNav").style.width = "0%";
}

function confirmdelete() {
    "use strict";
	var confirmed = confirm("Apakah anda yakin untuk menghapus post ini?");
	return confirmed;
}

function likepost(){
    var xhttp = new XMLHttpRequest();
    var like = Number(document.getElementsByClassName("count")[1].innerHTML);
    var nomor = Number(document.getElementById("post").innerHTML);
    var status = Number(document.getElementById("status").innerHTML);
    
    if(status == 0){
        like++;
    }
    else if(status == 1){
        like--;
    }
    xhttp.onreadystatechange=function(){
        if(xhttp.readyState == 4 && xhttp.status == 200){
            document.getElementsByClassName("count")[1].innerHTML = ""+like;
            
        }
    };
    
    if(status == 0){
        status = 1;
        xhttp.open("GET", "like.php?id="+Number(nomor), true);  
    }
    else{
        status = 0;
        xhttp.open("GET", "dislike.php?id="+Number(nomor), true);  
    }
    document.getElementById("status").innerHTML = ""+status;
    xhttp.send();
    
    
}
