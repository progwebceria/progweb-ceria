<?php
    session_start();
if(isset($_SESSION["USERNAME"])){
?>

<html>
    <head>
        <title>Dashboard Admin</title>
        <link type="text/css" rel="stylesheet" href="design.css">
        <script src="JavaScript.js"></script>
    </head>
    
    <body>
        <table class="profile_bar">
            <tr>
                <td>Welcome, <?php echo $_SESSION["FIRST_NAME"];?></td>
            </tr>
            <tr>
                <td><a href="admin-logout.php">Logout</a></td>
            </tr>
        </table>
        <h1>Dashboard Admin</h1>
        
        <div id="containernav">
            <ul>
                <li><a class="active" href="admin-home.php">HOME</a></li>
                <li><a href="admin_confirmation.php">POST CONFIRMATION</a></li>
                <li class="right">
                    <div id="search_bar">
                        <input type="search" id="input_text" name="googlesearch" placeholder="Search">
                        <button><img class="search_logo" src="search-256.png"></button>
                    </div>
                </li>
            </ul>
        </div>
        <?php
            if(isset($_GET['id'])){
                if($_GET['id'] == 0){
                    echo "<p class='notification_red'>Post sudah dihapus</p>";
                }
                else if($_GET['id'] == 1){
                    echo "<p class='home_notification'>Post sudah ditampilkan di website</p>";
                }
            }
            else{
                echo "";
            }
            
        ?>
        <?php
            require_once("database-admin.php");
            $user_array;
        $user_array = get_all_users();
        if(count($user_array) > 0):
				foreach($user_array as $row): ?>
				<div class="users_admin" id="<?php echo $row['nim']; ?>">
                    <img class="profile_photo_admin" src="<?php echo $row['profile_picture'];?>">
					<h3>NIM : <?php echo $row['nim']; ?></h3>
                    
                    <p>Nama Lengkap : <?php echo $row['first_name']; ?> <?php echo $row['last_name']; ?> </p>
                    <p>Prodi : <?php echo $row['prodi']; ?> </p>
                    <p>Email : <?php echo $row['email']; ?> </p>
                    <p>Google: https://plus.google.com<?php echo $row['google']; ?> | Twitter: https://twitter.com/<?php echo $row['twitter']; ?> | Facebook: https://facebook.com/<?php echo $row['facebook'];?></p>
                    <a href="edit.php?id=<?php echo $row['nim']; ?>" class="btn-edit">Edit Profile <?php echo $row['first_name'];?></a>
				</div>
			<?php endforeach; 
				else: ?>
				<h3>No users available/found!</h3>
			<?php endif;
        ?>
        
        <br>
        <h1>All Posts</h1>
        <?php
            require_once("database-admin.php");
            $post_array;
        $post_array = get_all_posts();
            rsort($post_array);
        if(count($post_array) > 0):
				foreach($post_array as $row): ?>
				
					<?php
                    echo'
                        <div class="item2">
                <img class=item2_photo src="'.$row['location'].'"/> 
                <div class="item2_description_layer">
                <a class="post_setting" href="delete_post_admin.php?id='.$row['id'].'" onclick="return confirmdelete()">Delete</a>
                    <a class="post_setting" href="edit_post_admin.php?id='.$row['id'].'">Edit</a>
                    <h3 class="item2_description">'.$row['title'].'</h3>;
                    <p class="item2_description">'.$row['desc'].'<a class="link" href="post.php?id='.$row['id'].'">See More..</a></p>
                </div> </div>
            
                    ';
                    ?>
        
			<?php endforeach; 
				else: ?>
				<h3>No posts available/found!</h3>
			<?php endif;
        ?>
    </body>
</html>
<?php }
else header("Location: admin.php");
?>