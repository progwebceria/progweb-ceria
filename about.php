<?php
    session_start();
include "database.php";
if(isset($_SESSION["NIM"])){
    $jumlah = count(get_notification_not_read($_SESSION["NIM"]));
}
?>
<!DOCTYPE html>

<html>
    <head>
        <title>UKDW Creative - About</title>
        <meta charset="UTF-8">
        <link type="text/css" rel="stylesheet" href="design.css">
        <script src="JavaScript.js"></script>
    </head>
    
    <body>
        <div id="containerheader">
            
            
            <a href="home.php"><img id="leftlogo" src="uc.png"/></a>
            <a href="plus.google.com"><img id="gplus" src="gplus-mono.png" onmouseover="mOverGoogle(this)" onmouseout="mOutGoogle(this)"/></a>
            <a href="twitter.com"><img id="twitter" src="twitter-mono.png" onmouseover="mOverTwitter(this)" onmouseout="mOutTwitter(this)"/></a>
            <a href="facebook.com"><img id="fb" src="fb-mono.png" onmouseover="mOverFacebook(this)" onmouseout="mOutFacebook(this)"/></a>
            <?php
            if(isset($_SESSION['NIM'])){
                echo '
                <div id="myNav" class="overlay">

  <!-- Button to close the overlay navigation -->
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">x</a>

  <!-- Overlay content -->
  <div class="overlay-content">';
                ?>
            <?php
            require_once("database.php");
            $notification_array;
        $notification_array = get_notification($_SESSION["NIM"]);
                rsort($notification_array);
        if(count($notification_array) > 0):
                
				foreach($notification_array as $row): ?>
				
					<?php
                
                    echo '<a href="post.php?id='.$row['post'].'"><img style="height:50px;width:80px;float:left;" src="'.$row['picture'].'">Mahasiswa dengan NIM '.$row['dari'].' telah '.$row['type'].' post anda!</a>';
                        
                    
                    
                    ?>
        
			<?php endforeach; 
				 ?>
				
			<?php endif;
        ?>
<?php
  echo '</div>

</div>
';
?>
<!-- Use any element to open/show the overlay navigation menu -->
            <?php
                if($jumlah > 0){
                    echo '<span id="notification_count" style="float:right">'.$jumlah.'</span>';
                }    
            ?>

            <?php echo'
<span onclick="openNav()" style="float:right;font-size:40px;margin:0px;">=</span>

                ';
            }
            ?>
            <table class="profile_bar">
                <tbody>
                    <tr>
                        <td rowspan="2"><img class="profile_picture" src="guest.png" alt="ProfilePicture"/></td>
                        <?php
                            echo "<td>Welcome, ";
                            if(isset($_SESSION['NIM'])){
                                echo '<a href="profile.php?nim=';
                                echo $_SESSION['NIM'];
                                echo '">';
                                echo $_SESSION['FIRST_NAME'];
                                echo '</a>';
                                echo'
                                <tr>
                                    <td><a href="logout.php">Logout</a></td>
                                </tr>';
                            }
                            else{
                                echo "Guest";
                                 echo'
                                <tr>
                                    <td><a href="login.php">Login</a></td>
                                </tr>';
                            }
                                
                            echo "</td>";
                        ?>
                    </tr>
                    
                    
                </tbody>
            </table>
            
        </div>
        <div id="containernav">
            <ul>
                <li><a href="home.php">HOME</a></li>
                <li><div class="dropdown">
                    <a href="#" class="dropbutton">PORTFOLIO</a>
                    <div class="dropdown-content">
                        <a href="category.php?type=websites">WEBSITES</a>
                        <a href="category.php?type=applications">APPLICATIONS</a>
                        <a href="category.php?type=multimedia">MULTIMEDIA</a>
                    </div>
                </div>
                </li>
                <li><a class="active" href="about.php">ABOUT</a></li>
                <li class="right">
                    <form id="search_bar" method="GET" action="search.php">
                        <input type="search" id="input_text" name="key" placeholder="Search">
                        <select name = "type">
                            <option value="all">All</option>
                            <option value="users">Users</option>
                            <option value="posts">Posts</option>
                        </select>
                        <button><img class="search_logo" src="search-256.png"></button>
                    </form>
                </li>
            </ul>
        </div>
        <div id="containersection">
            <h1>About</h1>
            <hr/>
            <p>UKDW Creative merupakan suatu website yang bertujuan untuk menampilkan hasil karya dari Keluarga Universitas Kristen Duta Wacana. Website ini dibuat pada tanggal 19 Februari 2016 dan hingga saat ini masih dalam tahap pengembangan.</p>
            
            <h1 class="center_text">The Crew</h1>
            <div class="crew">
                <img class="crewphoto" src="10679498_10202866420446992_1841943240523082379_o.jpg" alt="Crews"/>
                <div class="crewdescription">
                <h3>Anton Susilo</h3>
                <hr/>
                <p>Berbekal pengetahuan HTML, CSS dan Java Script, saya berperan sebagai pengembang utama
                website ini, terutama pada bagian <i>bug fixes</i>.</p>
                </div>
            </div>
            
            <div class="crew">
                <img class="crewphoto" src="Bitha1.jpg" alt="Crews"/>
                <div class="crewdescription">
                <h3>Tabitha Banu</h3>
                <hr/>
                <p>Saya fokus pada <i>graphic design</i> website ini. <i>Icons</i> pada <b>UKDW Creative</b> dibuat menggunakan
                <i>Adobe Illustrator</i> dan <i>Adobe Photoshop</i>.</p>
                </div>
            </div>

            <div class="crew">
                <img class="crewphoto" src="Ezra1.jpg" alt="Crews"/>
                <div class="crewdescription">
                <h3>Ezra Andriani</h3>
                <hr/>
                <p>Saya bertugas mengedit atau menambahkan <i>code</i> dalam website ini.</p>
                </div>
            </div>

            <div class="crew">
                <img class="crewphoto" src="Steishy.jpg" alt="Crews"/>
                <div class="crewdescription">
                <h3>Steishy Mega</h3>
                <hr/>
                <p>Saya bertugas mengoreksi <i>layout</i> dalam website ini.</p>
                </div>
            </div>
            
        </div>
        <div id="containerfooter">
            <hr>
            <table>
              <tr>
                <td><p id="footerkiri"><i>&copy; 2016 | UKDW Creative</i></p></td>
                <td><p id="footerkanan"><i>Progweb Ceria's Team</i></p></td>
              </tr>
            </table>
        </div>
    </body>
</html>