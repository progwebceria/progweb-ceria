<?php
function connect_database() {
	//konfigurasi database
	$db_user = "user";
	$db_password = "GJAPLn35ZTbam3qw";
	$db_name = "user";
	$db_host = "localhost";
	
	//akan selalu dilakukan, lebih baik jika ditulis dalam sebuah fungsi tersendiri
	$koneksi = mysqli_connect($db_host, $db_user, $db_password, $db_name);
	return $koneksi;
}

function get_admin($username) {
	$koneksi = connect_database();
	$sql = "SELECT password,first_name,last_name,email FROM admins WHERE username = ?";
	$stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "s", $username);
	mysqli_stmt_execute($stmt);
	mysqli_stmt_store_result($stmt);
	$admin = array();
	if(mysqli_stmt_num_rows($stmt)) {
		mysqli_stmt_bind_result($stmt, $password , $first_name, $last_name, $email);
		mysqli_stmt_fetch($stmt);

		//ubah ke bentuk array
		$admin = array("username" => $username, "password"=> $password, "first_name" => $first_name, "last_name" => $last_name, "email" => $email);
	}
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
	return $admin;
}

function get_user($nim) {
	$koneksi = connect_database();
	$sql = "SELECT password,first_name,last_name,prodi,email,google,twitter,facebook,profile_picture FROM users WHERE nim = ?";
	$stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "s", $nim);
	mysqli_stmt_execute($stmt);
	mysqli_stmt_store_result($stmt);
	$user = array();
	if(mysqli_stmt_num_rows($stmt)) {
		mysqli_stmt_bind_result($stmt, $password , $first_name, $last_name, $prodi, $email,$google,$twitter,$facebook,$profile_picture);
		mysqli_stmt_fetch($stmt);

		//ubah ke bentuk array
		$user = array("nim" => $nim, "password"=> $password, "first_name" => $first_name, "last_name" => $last_name, "prodi" => $prodi, "email" => $email,"google" => $google,"twitter" => $twitter,"facebook" => $facebook,"profile_picture" => $profile_picture);
	}
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
	return $user;
}

function get_all_users() {
	$koneksi = connect_database();
	$sql = "SELECT * FROM users";
	$result = mysqli_query($koneksi, $sql);

	//ubah ke bentuk array
	$user_array = array();
	while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
		$single_quote = array("nim" => $row['nim'], "password" => $row['password'], "first_name" => $row['first_name'], "last_name" => $row['last_name'], "prodi" => $row['prodi'], "email" => $row['email'], "google" => $row['google'], "twitter" => $row['twitter'], "facebook" => $row['facebook'], "profile_picture" => $row['profile_picture']);
		$user_array[] = $single_quote;
	}

	mysqli_close($koneksi);
	return $user_array;	
}

function update_user($id,$email,$google,$twitter,$facebook) {
	$koneksi = connect_database();
	
	//escape input
	$email = mysqli_real_escape_string($koneksi, $email);
    $google = mysqli_real_escape_string($koneksi, $google);
    $twitter = mysqli_real_escape_string($koneksi, $twitter);
    $facebook = mysqli_real_escape_string($koneksi, $facebook);
	$sql = "UPDATE users SET email = ? , google = ? , twitter = ? , facebook = ? WHERE nim = ?";
	$stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "sssss", $email,$google,$twitter,$facebook, $id);
	mysqli_stmt_execute($stmt);
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
}

function get_all_posts() {
	$koneksi = connect_database();
	$sql = "SELECT * FROM posts WHERE status=1";
	$result = mysqli_query($koneksi, $sql);

	//ubah ke bentuk array
	$post_array = array();
	while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
		$single_quote = array("id" => $row['id'], "title" => $row['title'], "nim" => $row['nim'], "type" => $row['type'], "desc" => $row['desc'], "location" => $row['location']);
		$post_array[] = $single_quote;
	}

	mysqli_close($koneksi);
	return $post_array;	
}

function get_all_pending_posts(){
    $koneksi = connect_database();
	$sql = "SELECT * FROM posts WHERE status=0";
	$result = mysqli_query($koneksi, $sql);

	//ubah ke bentuk array
	$post_array = array();
	while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
		$single_quote = array("id" => $row['id'], "title" => $row['title'], "nim" => $row['nim'], "type" => $row['type'], "desc" => $row['desc'], "location" => $row['location']);
		$post_array[] = $single_quote;
	}

	mysqli_close($koneksi);
	return $post_array;
}


function update_post($id,$title,$type,$desc,$location) {
	$koneksi = connect_database();

	//escape input
	$title = mysqli_real_escape_string($koneksi, $title);
    $type = mysqli_real_escape_string($koneksi, $type);
    $desc = mysqli_real_escape_string($koneksi, $desc);
    $location = mysqli_real_escape_string($koneksi, $location);

	$sql = "UPDATE `posts` SET `title` = ? , `type` = ? , `desc` = ? , `location` = ? WHERE `id` = ?";
	$stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "ssssi", $title,$type,$desc,$location,$id);
	mysqli_stmt_execute($stmt);
	$newid = mysqli_insert_id($koneksi);
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
	return $newid;
}

function get_post($id) {
	$koneksi = connect_database();
	$sql = "SELECT `title`,`nim`,`type`,`desc`,`location` FROM `posts` WHERE id = ?";
	$stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "i", $id);
	mysqli_stmt_execute($stmt);
	mysqli_stmt_store_result($stmt);
	$user = array();
	if(mysqli_stmt_num_rows($stmt)) {
		mysqli_stmt_bind_result($stmt, $title, $nim, $type, $desc, $location);
		mysqli_stmt_fetch($stmt);

		//ubah ke bentuk array
		$post = array("title" => $title, "nim"=> $nim, "type" => $type, "desc" => $desc, "location" => $location);
	}
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
	return $post;
}


function delete_post($id) {
	$koneksi = connect_database();
	$sql = "DELETE FROM posts WHERE id = ?";
	$stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "i", $id);
	mysqli_stmt_execute($stmt);
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
}

function accept_post($id){
    $koneksi = connect_database();
    $sql = "UPDATE `posts` SET `status` = 1 WHERE `id` = ?";
	$stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "i", $id);
    mysqli_stmt_execute($stmt);
	$newid = mysqli_insert_id($koneksi);
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
	return $newid;
}


?>