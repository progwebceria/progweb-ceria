<!DOCTYPE html>

<?php
$id = $_GET['id'];
require("database.php");
$post = get_post($id);
session_start();
if(!isset($_SESSION["USERNAME"])){
    header("Location : admin.php");
}

?>

<html>
    <head>
        <title>Edit Post <?php echo $post['title'];?></title>
        <link type="text/css" rel="stylesheet" href="design.css">
        <script src="JavaScript.js"></script>
    </head>
    
    <body>
        <h1>Edit Post <?php echo $post['title'];?></h1>
        <div id="containersection">
            <form method="POST" action="edit_post_admin_process.php" enctype="multipart/form-data">
                <input type="text" name="id" id="id" value="<?php echo $id;?>" hidden="hidden">
                <img class="profile_photo_size" src="<?php echo $post['location'];?>"/><br>
                Change your Post Picture :
                <br>
                <input type="file" name="upload" id="file">
                <br>
                Category: <br>
                <?php 
                if($post['type'] == "websites"){
                    
                echo '
                <input type="radio" name="type" value="websites" checked="checked"> Websites <br>
                <input type="radio" name="type" value="applications"> Applications <br>
                <input type="radio" name="type" value="multimedia"> Multimedia <br>
                ';
                }
                else if($post['type'] == "applications") {
                echo '
                <input type="radio" name="type" value="websites"> Websites <br>
                <input type="radio" name="type" value="applications" checked="checked"> Applications <br>
                <input type="radio" name="type" value="multimedia"> Multimedia <br>
                ';
                }
                else{
                   echo '
                <input type="radio" name="type" value="websites"> Websites <br>
                <input type="radio" name="type" value="applications" checked="checked"> Applications <br>
                <input type="radio" name="type" value="multimedia" checked="checked"> Multimedia <br>
                '; 
                }
                ?>
                <br>
                <label>Title:</label><br>
				<input type="text" name="title" value="<?php echo $post['title']; ?>"><br>
				
                <label>Description:</label><br>
                <textarea rows="5" cols="50 "name="description" id="description"><?php echo $post['desc']; ?></textarea><br>
				<input type="submit" name="Submit"> | <a href="admin-home.php">Cancel</a>
			</form>
        </div>
    </body>
</html>