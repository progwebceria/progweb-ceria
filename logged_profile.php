<!DOCTYPE html>

<html>
    <head>
        <title>UKDW Creative - Profile</title>
        <link type="text/css" rel="stylesheet" href="design.css">
    </head>
    
    <body>
        <div id="containerheader">
            <a href="logged_home.php"><img id="leftlogo" src="uc.png"/></a>
            <a href="plus.google.com"><img id="gplus" src="gplus-warna.png"/></a>
            <a href="twitter.com"><img id="twitter" src="twitter-warna.png"/></a>
            <a href="facebook.com"><img id="fb" src="fb-warna.png"/></a>
            <table class="profile_bar">
                <tbody>
                    <tr>
                        <td rowspan="2"><img class="profile_picture" src="guest.png"/></td>
                        <td>Welcome,Anton</td>
                    </tr>
                    <tr>
                        <td><a href="logout.php">Logout</a></td>
                    </tr>
                    
                </tbody>
            </table>
        </div>
        <div id="containernav">
            <ul>
                <li><a href="logged_home.php">HOME</a></li>
                <li><div class="dropdown">
                    <a class="active" href="#" class="dropbutton">PORTFOLIO</a>
                    <div class="dropdown-content">
                        <a href="logged_websites.php">WEBSITES</a>
                        <a href="logged_applications.php">APPLICATIONS</a>
                        <a href="logged_multimedia.php">MULTIMEDIA</a>
                    </div>
                </div>
                </li>
                <li><a href="logged_about.php">ABOUT</a></li>
                <li class="right">
                    <div id="search_bar">
                        <input type="search" id="input_text" name="googlesearch" placeholder="Search">
                        <button><img class="search_logo" src="search-256.png"></button>
                    </div>
                </li>
            </ul>
        </div>
        <div id="containersection">
            <div class="profile-page">
                <h2>Anton Susilo</h2>
            <div class="tepi">
                <div class="profile_photo">
                    <img class="profile_photo_size" src="taking-photo-hiking-sport-adventure.jpg"/>
                    <p class="nomargin">Find Anton in :</p>
                        <a href="plus.google.com"><img id="gplus" src="gplus-warna.png"/></a>
                        <a href="twitter.com"><img id="twitter" src="twitter-warna.png"/></a>
                        <a href="facebook.com"><img id="fb" src="fb-warna.png"/></a>
                    
                </div>
                <br>
                <p>Prodi : Teknik Informatika</p>
                <p>Angkatan : 2014</p>
                <p>About me: </p>
                <p>Saya adalah mahasiswa Teknik Informatika Universitas Kristen Duta Wacana Angkatan 2014</p>
            </div>

            <div class="keterangan">
                <div class="keterangan-detail">
                    <img class="logo-keterangan" src="4cbKLKxdi.png">
                    <p class="nomargin">Posts</p>
                    <p class = "jumlah nomargin">1</p>
                </div>
                <div class="keterangan-detail">
                    <img class="logo-keterangan" src="favourite.png">
                    <p class="nomargin">Likes</p>
                    <p class = "jumlah nomargin">30</p>
                </div>
                <div class="keterangan-detail">
                    <img class="logo-keterangan" src="comment.png">
                    <p class="nomargin">Comments</p>
                    <p class = "jumlah nomargin">1</p>
                </div>
                <div class="keterangan-detail">
                    <img class="logo-keterangan" src="mata.png">
                    <p class="nomargin">Seen</p>
                    <p class = "jumlah nomargin">1.2K</p>
                </div>
                
                <div class="item2_in_profile">
            <img class=item2_photo src="characters-red.png"/> 
            
            <div class="item2_description_layer">
                <h3 class="item2_description">Angry Bird</h3>
                <p class="item2_description nomargin">Angry Birds adalah permainan video yang pada awalnya tersedia untuk aplikasi iPad dan iPhone, namun kini telah dirilis di berbagai media. <a class="link" href="angry_birds.php">See More..</a></p>
                <img class="edit" src="editsvg.png">
            </div>
        </div>
                
            </div>    
        </div>
            <div class="spacing_bawah">
            </div>
        </div>
        <div id="containerfooter">
            <hr>
            <table>
              <tr>
                <td><p id="footerkiri"><i>&copy 2016 | UKDW Creative</i></p></td>
                <td><p id="footerkanan"><i>Progweb Ceria's Team</i></p></td>
              </tr>
            </table>
        </div>
    </body>
</html>