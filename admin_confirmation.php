<?php
session_start();
?>

<html>

    <head>
        <title>Post Confirmation</title>
        <link type="text/css" rel="stylesheet" href="design.css">
        <script src="JavaScript.js"></script>
    </head>
    
    <body>
<table class="profile_bar">
            <tr>
                <td>Welcome, <?php echo $_SESSION["FIRST_NAME"];?></td>
            </tr>
            <tr>
                <td><a href="admin-logout.php">Logout</a></td>
            </tr>
        </table>
        <h1>Dashboard Admin</h1>
        
        <div id="containernav">
            <ul>
                <li><a href="admin-home.php">HOME</a></li>
                <li><a class="active" href="admin_confirmation.php">POST CONFIRMATION</a></li>
                <li class="right">
                    <div id="search_bar">
                        <input type="search" id="input_text" name="googlesearch" placeholder="Search">
                        <button><img class="search_logo" src="search-256.png"></button>
                    </div>
                </li>
            </ul>
        </div>
        
        <table border="1">
            
<?php
            require_once("database-admin.php");
            $post_array;
        $post_array = get_all_pending_posts();
            rsort($post_array);

        if(count($post_array) > 0):
            echo '<thead>
                <td><p style="padding-left:100px; padding-right:100px;">Picture</p></td>
                <td><p style="padding-left:80px; padding-right:80px;">Title</p></td>
                <td><p style="padding-left:80px; padding-right:80px;">NIM</p></td>
                <td><p style="padding-left:75px;padding-right:50px;">Description</p></td>
            </thead>';
				foreach($post_array as $row): ?>
		
					<?php
                    echo'
                        <tr>
                <td><img class=photo_small src="'.$row['location'].'"/></td>
                <td>'.$row['title'].'</td>
                <td>'.$row['nim'].'</td>
                
                <td><p style="margin:0px;width:250px;">'.$row['desc'].'</p></td>
                <td><a href="accept.php?id='.$row['id'].'">Accept</a></td>
                <td><a href="delete_post_admin.php?id='.$row['id'].'">Delete</a></td>
                    ';
                    ?>
        
			<?php endforeach; 
				else: ?>
				<h3>No posts available/found!</h3>
			<?php endif;
        ?>
            </table>
    </body>
    
</html>