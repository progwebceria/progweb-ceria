<!DOCTYPE html>

<?php
    session_start();
    $nim = $_GET['nim'];
    require("database.php");
    $user = get_user($nim);

    if(isset($_SESSION["NIM"])){
    $jumlah = count(get_notification_not_read($_SESSION["NIM"]));
    
    $jumlah_post = count(get_user_posts($nim));
}
?>

<html>
    <head>
        <title>UKDW Creative - Profile</title>
        <link type="text/css" rel="stylesheet" href="design.css">
        <script src="JavaScript.js"></script>
    </head>
    
    <body>
        <div id="containerheader">
            <a href="home.php"><img id="leftlogo" src="uc.png"/></a>
            <a href="plus.google.com"><img id="gplus" src="gplus-mono.png" onmouseover="mOverGoogle(this)" onmouseout="mOutGoogle(this)"/></a>
            <a href="twitter.com"><img id="twitter" src="twitter-mono.png" onmouseover="mOverTwitter(this)" onmouseout="mOutTwitter(this)"/></a>
            <a href="facebook.com"><img id="fb" src="fb-mono.png" onmouseover="mOverFacebook(this)" onmouseout="mOutFacebook(this)"/></a>
            <?php
            if(isset($_SESSION['NIM'])){
                echo '
                <div id="myNav" class="overlay">

  <!-- Button to close the overlay navigation -->
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">x</a>

  <!-- Overlay content -->
  <div class="overlay-content">';
                ?>
            <?php
            require_once("database.php");
            $notification_array;
        $notification_array = get_notification($_SESSION["NIM"]);
                rsort($notification_array);
        if(count($notification_array) > 0):
                
				foreach($notification_array as $row): ?>
				
					<?php
                
                    echo '<a href="post.php?id='.$row['post'].'"><img style="height:50px;width:80px;float:left;" src="'.$row['picture'].'">Mahasiswa dengan NIM '.$row['dari'].' telah '.$row['type'].' post anda!</a>';
                        
                    
                    
                    ?>
        
			<?php endforeach; 
				 ?>
				
			<?php endif;
        ?>
<?php
  echo '</div>

</div>
';
?>
<!-- Use any element to open/show the overlay navigation menu -->
            <?php
                if($jumlah > 0){
                    echo '<span id="notification_count" style="float:right">'.$jumlah.'</span>';
                }    
            ?>

            <?php echo'
<span onclick="openNav()" style="float:right;font-size:40px;margin:0px;">=</span>

                ';
            }
            ?>
            <table class="profile_bar">
                <tbody>
                    <tr>
                        <td rowspan="2"><img class="profile_picture" src="guest.png"/></td>
                        <?php 
                            echo "<td>Welcome, ";
                            if(isset($_SESSION['NIM'])){
                                echo '<a href="profile.php?nim=';
                                echo $_SESSION['NIM'];
                                echo '">';
                                echo $_SESSION['FIRST_NAME'];
                                echo '</a>';
                                echo'
                                <tr>
                                    <td><a href="logout.php">Logout</a></td>
                                </tr>';
                            }
                            else{
                                echo '<td>Guest</td>';
                                echo '<tr>
                        <td><a href="login.php">Login</a></td>
                    </tr>';
                            }
                        ?> 
                    </tr>
                    
                    
                </tbody>
            </table>
        </div>
        <div id="containernav">
            <ul>
                <li><a href="home.php">HOME</a></li>
                <li><div class="dropdown">
                    <a class="active" href="#" class="dropbutton">PORTFOLIO</a>
                    <div class="dropdown-content">
                        <a href="category.php?type=websites">WEBSITES</a>
                        <a href="category.php?type=applications">APPLICATIONS</a>
                        <a href="category.php?type=multimedia">MULTIMEDIA</a>
                    </div>
                </div>
                </li>
                <li><a href="about.php">ABOUT</a></li>
                <li class="right">
                    <form id="search_bar" method="GET" action="search.php">
                        <input type="search" id="input_text" name="key" placeholder="Search">
                        <select name = "type">
                            <option value="all">All</option>
                            <option value="users">Users</option>
                            <option value="posts">Posts</option>
                        </select>
                        <button><img class="search_logo" src="search-256.png"></button>
                    </form>
                </li>
            </ul>
        </div>
        <div id="containersection">
            <div class="profile-page">
                <h2><?php echo $user['first_name'];?> <?php echo $user['last_name'];?></h2>
                <?php
                if(isset($_SESSION["NIM"]) && $_SESSION["NIM"] == $user['nim']){
                    echo '<a href = "edit_profile.php" style="float: left;">Edit your Profile</a>';
                }
                ?>
                
                <br>
            <div class="tepi">
                <div class="profile_photo">
                    <img class="profile_photo_size" src="<?php echo $user['profile_picture'];?>"/>
                    <p class="nomargin">Find <?php echo $user['first_name']; ?> in :</p>
                    <?php
                    if($user['google'] != ""){
                        echo '<a href="https://plus.google.com/';
                        echo $user['google'];
                        echo '" target="_blank"/>';
                        echo '<img id="gplus" src="gplus-warna.png"/></a>';
                    }
                    if($user['twitter'] != ""){
                        echo '<a href="https://twitter.com/';
                        echo $user['twitter'];
                        echo '" target="_blank"/>';
                        echo '<img id="twitter" src="twitter-warna.png"/></a>';
                    }
                    if($user['facebook'] != ""){
                        echo '<a href="https://facebook.com/';
                        echo $user['facebook'];
                        echo '" target="_blank"/>';
                        echo '<img id="fb" src="fb-warna.png"/></a>';
                    }
                    ?>
                    
                </div>
                <br>
                <p>Nama Lengkap : <?php echo $user['first_name']; ?> <?php echo $user['last_name']; ?></p>
                <p>Prodi : <?php echo $user['prodi']; ?></p>
                <p>Email : <?php echo $user['email']; ?></p>
            </div>
                

            <div class="keterangan">
                <div class="keterangan-detail">
                    <img class="logo-keterangan" src="4cbKLKxdi.png">
                    <p class="nomargin">Posts</p>
                    <p class = "jumlah nomargin"><?php echo $jumlah_post;?></p>
                </div>
                <div class="keterangan-detail">
                    <img class="logo-keterangan" src="favourite.png">
                    <p class="nomargin">Likes</p>
                    <p class = "jumlah nomargin"><?php echo $user['liked']; ?></p>
                </div>
                <div class="keterangan-detail">
                    <img class="logo-keterangan" src="comment.png">
                    <p class="nomargin">Comments</p>
                    <p class = "jumlah nomargin"><?php echo $user['commended']; ?></p>
                </div>
                <div class="keterangan-detail">
                    <img class="logo-keterangan" src="mata.png">
                    <p class="nomargin">Seen</p>
                    <p class = "jumlah nomargin"><?php echo $user['viewed']; ?></p>
                </div>
                
                
            </div> 
                
                <?php
            require_once("database.php");
            $post_array;
        $post_array = get_user_posts($nim);
                rsort($post_array);
        if(count($post_array) > 0):
				foreach($post_array as $row): ?>
				
					<?php
                if(isset($_SESSION["NIM"])){
                    if($_SESSION["NIM"] == $user['nim']){
                        echo'
                        <div class="item2">
                <img class=item2_photo src="'.$row['location'].'"/> 
                <div class="item2_description_layer">
                    <a class="post_setting" href="delete_post.php?id='.$row['id'].'" onclick="return confirmdelete()">Delete</a>
                    <a class="post_setting" href="edit_post.php?id='.$row['id'].'">Edit</a>
                    
                    <h3 class="item2_description">'.$row['title'].'</h3>;
                    <p class="item2_description">'.$row['desc'].'<a class="link" href="post.php?id='.$row['id'].'">See More..</a></p>
                </div> </div>
            
                    ';
                    }
                    else{
                        echo'
                        <div class="item2">
                <img class=item2_photo src="'.$row['location'].'"/> 
                <div class="item2_description_layer">
                    <h3 class="item2_description">'.$row['title'].'</h3>;
                    <p class="item2_description">'.$row['desc'].'<a class="link" href="post.php?id='.$row['id'].'">See More..</a></p>
                </div> </div>
            
                    ';
                    }
                    
                }
                else{
                    echo'
                        <div class="item2">
                <img class=item2_photo src="'.$row['location'].'"/> 
                <div class="item2_description_layer">
                    <h3 class="item2_description">'.$row['title'].'</h3>;
                    <p class="item2_description">'.$row['desc'].'<a class="link" href="post.php?id='.$row['id'].'">See More..</a></p>
                </div> </div>
            
                    ';
                }
                    
                    ?>
        
			<?php endforeach; 
				else: ?>
				<h3>No posts available/found!</h3>
			<?php endif;
        ?>
        </div>
        </div>
        <div id="containerfooter">
            <hr>
            <table>
              <tr>
                <td><p id="footerkiri"><i>&copy; 2016 | UKDW Creative</i></p></td>
                <td><p id="footerkanan"><i>Progweb Ceria's Team</i></p></td>
              </tr>
            </table>
        </div>
    </body>
</html>