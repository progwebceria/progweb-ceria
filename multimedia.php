
<?php
    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>UKDW Creative - Multimedia</title>
        <link type="text/css" rel="stylesheet" href="design.css">
    </head>
    
    <body>
        <div id="containerheader">
            
            <a href="home.php"><img id="leftlogo" src="uc.png"/></a>
            <a href="plus.google.com"><img id="gplus" src="gplus-warna.png"/></a>
            <a href="twitter.com"><img id="twitter" src="twitter-warna.png"/></a>
            <a href="facebook.com"><img id="fb" src="fb-warna.png"/></a>
            
            <table class="profile_bar">
                <tbody>
                    <tr>
                        <td rowspan="2"><img class="profile_picture" src="guest.png"/></td>
                        <?php
                            echo "<td>Welcome, ";
                            if(isset($_SESSION['NIM'])){
                                echo '<a href="profile.php?nim=';
                                echo $_SESSION['FIRST_NAME'];
                                echo '">';
                                echo $_SESSION['FIRST_NAME'];
                                echo '</a>';
                                echo'
                                <tr>
                                    <td><a href="logout.php">Logout</a></td>
                                </tr>';
                            }
                            else{
                                echo "Guest";
                                 echo'
                                <tr>
                                    <td><a href="login.php">Login</a></td>
                                </tr>';
                            }
                                
                            echo "</td>";
                        ?>
                    </tr>
                    
                    
                </tbody>
            </table>
        </div>
        <div id="containernav">
            <ul>
                <li><a href="home.php">HOME</a></li>
                <li><div class="dropdown">
                    <a class="active" href="#" class="dropbutton">PORTFOLIO</a>
                    <div class="dropdown-content">
                        <a href="websites.php">WEBSITES</a>
                        <a href="applications.php">APPLICATIONS</a>
                        <a href="multimedia.php">MULTIMEDIA</a>
                    </div>
                </div>
                </li>
                <li><a href="about.php">ABOUT</a></li>
                <li class="right">
                    <div id="search_bar">
                        <input type="search" id="input_text" name="googlesearch" placeholder="Search">
                        <button><img class="search_logo" src="search-256.png"></button>
                    </div>
                </li>
            </ul>
        </div>
        <div id="containersection">
            <h1 class="most_liked nomargin">Multimedia</h1>
            <div class="subcategory_container">
                <div class="subcategory_tengah">
                    <a href="music.php"><div class="subcategory">
                        <img class="subcategory_image" src="2000px-Simple_Music.png"/>
                        <h2 class="subcategory_description">Music</h2>
                        <p class="subcategory_description">Dengarkan lagu hasil karya mahasiswa UKDW yang penuh dengan makna</p>
                    </div></a>
                    <a href="video.php"><div class="subcategory">
                        <img class="subcategory_image" src="Video.png"/>
                        <h2 class="subcategory_description">Video</h2>
                        <p class="subcategory_description">Tonton hasil karya mahasiswa UKDW yang penuh dengan makna</p>
                    </div></a>
                    <a href="picture.php"><div class="subcategory">
                        <img class="subcategory_image" src="my pictures.png"/>
                        <h2 class="subcategory_description">Picture</h2>
                        <p class="subcategory_description">Nikmati gambar hasil karya mahasiswa UKDW yang penuh dengan makna</p>
                    </div></a>
                </div>
            </div>
            <br>
            <h1 class="nomargin">All Multimedia Posts</h3>
            <div class="sort_by_container">
            <div class="sort_by">
                <div class="sort_button active">
                    <img class="sort_logo_size" src="hot.png"/>
                    <p class="sort_desc">Most Favorited</p>
                </div>
            </div>
            <div class="sort_by">
                <div class="sort_button">
                    <img class="sort_logo_size" src="favourite.png"/>
                    <p class="sort_desc">Most Commented</p>
                </div>
            </div>
            <div class="sort_by">
                <div class="sort_button">
                    <img class="sort_logo_size" src="recent.png"/>
                    <p class="sort_desc">Newest</p>
                </div>
            </div>
            </div>
            <br/>
            <div class="item2_container">
            <div class="item2">
            <img class=item2_photo src="2180379_500.jpg"/> 
            <div class="item2_description_layer">
                <h3 class="item2_description">Lay Back</h3>
                <p class="item2_description">Lay Back merupakan lagu yang dibuat oleh K.Will di Album ketiganya. Dengarlah lagunya yang penuh dengan romantisme dan juga penuh makna ini</p>
            <audio controls style="width:339px;">
                <source src=04%20Lay%20back.mp3 type="audio/mpeg">
                Your browser does not support the audio element.
            </audio>
            </div>
            
        </div>
            
            
        </div>
            <div class="spacing_bawah">
            </div>
        </div>
        <div id="containerfooter">
            <hr>
            <table>
              <tr>
                <td><p id="footerkiri"><i>&copy 2016 | UKDW Creative</i></p></td>
                <td><p id="footerkanan"><i>Progweb Ceria's Team</i></p></td>
              </tr>
            </table>
        </div>
    </body>
</html>