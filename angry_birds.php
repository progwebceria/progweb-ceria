<!DOCTYPE html>
<?php
    session_start();
?>
<html>
    <head>
        <title>UKDW Creative - Angry Birds</title>
        <link type="text/css" rel="stylesheet" href="design.css">
    </head>
    
    <body>
        <div id="containerheader">
            <a href="home.php"><img id="leftlogo" src="uc.png"/></a>
            <a href="plus.google.com"><img id="gplus" src="gplus-warna.png"/></a>
            <a href="twitter.com"><img id="twitter" src="twitter-warna.png"/></a>
            <a href="facebook.com"><img id="fb" src="fb-warna.png"/></a>
            <table class="profile_bar">
                <tbody>
                    <tr>
                        <td rowspan="2"><img class="profile_picture" src="guest.png"/></td>
                        <?php 
                            if(isset($_SESSION["NIM"])){
                                echo '<td>Welcome,';
                                echo $_SESSION["NIM"];
                                echo '</td>';
                                echo '<tr>
                        <td><a href="logout.php">Logout</a></td>
                    </tr>';
                            }
                            else{
                                echo '<td>Welcome,Guest</td>';
                                echo '<tr>
                        <td><a href="login.php">Login</a></td>
                    </tr>';
                            }
                        ?> 
                    </tr>
                    
                </tbody>
            </table>
        </div>
        <div id="containernav">
            <ul>
                <li><a class="active" href="home.php">HOME</a></li>
                <li><div class="dropdown">
                    <a href="#" class="dropbutton">PORTFOLIO</a>
                    <div class="dropdown-content">
                        <a href="websites.php">WEBSITES</a>
                        <a href="applications.php">APPLICATIONS</a>
                        <a href="multimedia.php">MULTIMEDIA</a>
                    </div>
                </div>
                </li>
                <li><a href="about.php">ABOUT</a></li>
                <li class="right">
                    <div id="search_bar">
                        <input type="search" id="input_text" name="googlesearch" placeholder="Search">
                        <button><img class="search_logo" src="search-256.png"></button>
                    </div>
                </li>
            </ul>
        </div>
        <div id="containersection">
            <h1>Angry Birds</h1>
            <p class="title">Category : <a href="multimedia.php">Multimedia</a></p>
            <p class="maker">Posted by : <a href="profile.php?nim=informatika">Informatika 2014</a></p>
            <a href="plus.google.com"><img id="gplus" src="gplus-warna.png" title="Anton Susilo"/></a>
            <a href="twitter.com"><img id="twitter" src="twitter-warna.png" title="Follow @LJ_Liong"/></a>
            <a href="facebook.com"><img id="fb" src="fb-warna.png" title="Anton Susilo"/></a>
            <br>
            <br>
            <br>
            <hr/>
            <div class="item2_detail_profile">
                <img class="image_detail" src="characters-red.png">
                <p>Angry Birds adalah permainan video yang pada awalnya tersedia untuk aplikasi iPad dan iPhone, namun kini telah dirilis di berbagai media. Permainan ini membuat ketagihan penggunanya sehingga telah diunduh lebih dari 1 miliar pengguna. Bahkan pejabat Inggris pun tertarik dengan permainan ini.</p>
            </div>
            <div class="interaction_tab">
                <div class="comments">
                    <p class="count">1</p>
                    <img class="interaction" src="comment.png"/>
                </div>
                
                <div class="comments">
                    <p class="count">30</p>
                    <img class="interaction" src="favourite.png"/>
                </div>

                <div class="comments">
                    <p class="count">1.2K</p>
                    <img class="interaction" src="mata.png"/>
                </div>
                
            </div>
            <div class="write_comment">
            <?php
                if(isset($_SESSION["NIM"])){
                    echo '<h3>Comment as :</h3>';
                    echo '<h3>Nama : ';
                    echo $_SESSION["FIRST_NAME"];
                    echo '</h3>';
                    echo '<h3>NIM : ';
                    echo $_SESSION["NIM"];
                    echo '</h3>';
                    echo '
                    <form>
                    Comment : <br />
                <textarea rows="5" cols="50" name="description">
                
                </textarea>
                <button type="submit">Submit</button>
                </form>';
                } else{
                    echo '<h3>Comment as Guest:</h3>
            
                <form>
              Nama Lengkap
              <input type="text" name="fullname" placeholder="Masukkan Nama Lengkap"><br>
              Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <input type="text" name="email" placeholder="example@email.com">
                <br>
                Comment : <br />
                <textarea rows="5" cols="50" name="description">
                
                </textarea>
                    
                </form>
                <button type="submit">Submit</button>';
                }
            ?>  
            </div>
            
            
            
            <div class="comments_container">
                <div class="user_comment">
                    <div class="comment_photo">
                        <img class="comment_photo_size" src="guest.png">
                    </div>
                    <div class="comment_content">
                        <h5 class="comment_date_and_time">22 Februari 2016 (23:59 WIB)</h5>
                        <h2 class="comment_name">Gunawan Tibi</h2>
                        <h4 class="comment_name">Email : tibiisejatii@gmail.com</h4>
                        <hr>
                        <p>Fotonya bagus banget .. Sering-sering bikin graphic kayak gini lagi ya gan,sukses lalu haha..</p>
                    </div>
                </div>
            </div>
        </div>
        <div id="containerfooter">
            <hr>
            <table>
              <tr>
                <td><p id="footerkiri"><i>&copy 2016 | UKDW Creative</i></p></td>
                <td><p id="footerkanan"><i>Progweb Ceria's Team</i></p></td>
              </tr>
            </table>
        </div>
    </body>
</html>