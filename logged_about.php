<!DOCTYPE html>

<html>
    <head>
        <title>UKDW Creative - About</title>
        <link type="text/css" rel="stylesheet" href="design.css">
    </head>
    
    <body>
        <div id="containerheader">
            
            <a href="logged_home.php"><img id="leftlogo" src="uc.png"/></a>
            <a href="plus.google.com"><img id="gplus" src="gplus-warna.png"/></a>
            <a href="twitter.com"><img id="twitter" src="twitter-warna.png"/></a>
            <a href="facebook.com"><img id="fb" src="fb-warna.png"/></a>
            
            <table class="profile_bar">
                <tbody>
                    <tr>
                        <td rowspan="2"><img class="profile_picture" src="guest.png"/></td>
                        <td>Welcome,Anton</td>
                    </tr>
                    <tr>
                        <td><a href="logout.php">Logout</a></td>
                    </tr>
                    
                </tbody>
            </table>
            
        </div>
        <div id="containernav">
            <ul>
                <li><a href="logged_home.php">HOME</a></li>
                <li><div class="dropdown">
                    <a href="#" class="dropbutton">PORTFOLIO</a>
                    <div class="dropdown-content">
                        <a href="logged_websites.php">WEBSITES</a>
                        <a href="logged_applications.php">APPLICATIONS</a>
                        <a href="logged_multimedia.php">MULTIMEDIA</a>
                    </div>
                </div>
                </li>
                <li><a class="active" href="about.php">ABOUT</a></li>
                <li class="right">
                    <div id="search_bar">
                        <input type="search" id="input_text" name="googlesearch" placeholder="Search">
                        <button><img class="search_logo" src="search-256.png"></button>
                    </div>
                </li>
            </ul>
        </div>
        <div id="containersection">
            <h1>About</h1>
            <hr/>
            <p>UKDW Creative merupakan suatu website yang bertujuan untuk menampilkan hasil karya dari Keluarga Universitas Kristen Duta Wacana. Website ini dibuat pada tanggal 19 Februari 2016 dan hingga saat ini masih dalam tahap pengembangan.</p>
            
            <h1 class="center_text">The Crew</h1>
            <div class="crew">
                <img class="crewphoto" src="10679498_10202866420446992_1841943240523082379_o.jpg"/>
                <div class="crewdescription">
                <h3>Anton Susilo</h3>
                <hr/>
                <p>Berbekal pengetahuan HTML, CSS dan Java Script, saya berperan sebagai pengembang utama
                website ini, terutama pada bagian <i>bug fixes</i>.</p>
                </div>
            </div>
            
            <div class="crew">
                <img class="crewphoto" src="Bitha1.jpg"/>
                <div class="crewdescription">
                <h3>Tabitha Banu</h3>
                <hr/>
                <p>Saya fokus pada <i>graphic design</i> website ini. <i>Icons</i> pada <b>UKDW Creative</b> dibuat menggunakan
                <i>Adobe Illustrator</i> dan <i>Adobe Photoshop</i>.</p>
                </div>
            </div>

            <div class="crew">
                <img class="crewphoto" src="Ezra1.jpg"/>
                <div class="crewdescription">
                <h3>Ezra Andriani</h3>
                <hr/>
                <p>Saya bertugas mengedit atau menambahkan <i>code</i> dalam website ini.</p>
                </div>
            </div>

            <div class="crew">
                <img class="crewphoto" src="Steishy.jpg"/>
                <div class="crewdescription">
                <h3>Steishy Mega</h3>
                <hr/>
                <p>Saya bertugas mengoreksi <i>layout</i> dalam website ini.</p>
                </div>
            </div>
            
        </div>
        <div id="containerfooter">
            <hr>
            <table>
              <tr>
                <td><p id="footerkiri"><i>&copy 2016 | UKDW Creative</i></p></td>
                <td><p id="footerkanan"><i>Progweb Ceria's Team</i></p></td>
              </tr>
            </table>
        </div>
    </body>
</html>