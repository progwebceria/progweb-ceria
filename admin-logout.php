<?php
session_start();
unset($_SESSION["NIM"]);
unset($_SESSION["FIRST_NAME"]);
unset($_SESSION["LAST_NAME"]);
unset($_SESSION["EMAIL"]);
unset($_SESSION["STATUS"]);
header("Location: admin.php");
?>