
<?php
session_start();
$type = $_GET['type'];
include "database.php";
if(isset($_SESSION["NIM"])){
    $jumlah = count(get_notification_not_read($_SESSION["NIM"]));
}
?>
<!DOCTYPE html>

<html>
    <head>
        <title>UKDW Creative - <?php echo $type; ?></title>
        <link type="text/css" rel="stylesheet" href="design.css">
        <script src="JavaScript.js"></script>
    </head>
    
    <body>
        <div id="containerheader">
            
            <a href="home.php"><img id="leftlogo" src="uc.png"/></a>
            <a href="plus.google.com"><img id="gplus" src="gplus-mono.png" onmouseover="mOverGoogle(this)" onmouseout="mOutGoogle(this)"/></a>
            <a href="twitter.com"><img id="twitter" src="twitter-mono.png" onmouseover="mOverTwitter(this)" onmouseout="mOutTwitter(this)"/></a>
            <a href="facebook.com"><img id="fb" src="fb-mono.png" onmouseover="mOverFacebook(this)" onmouseout="mOutFacebook(this)"/></a>
            <?php
            if(isset($_SESSION['NIM'])){
                echo '
                <div id="myNav" class="overlay">

  <!-- Button to close the overlay navigation -->
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">x</a>

  <!-- Overlay content -->
  <div class="overlay-content">';
                ?>
            <?php
            require_once("database.php");
            $notification_array;
        $notification_array = get_notification($_SESSION["NIM"]);
                rsort($notification_array);
        if(count($notification_array) > 0):
                
				foreach($notification_array as $row): ?>
				
					<?php
                
                    echo '<a href="post.php?id='.$row['post'].'"><img style="height:50px;width:80px;float:left;" src="'.$row['picture'].'">Mahasiswa dengan NIM '.$row['dari'].' telah '.$row['type'].' post anda!</a>';
                        
                    
                    
                    ?>
        
			<?php endforeach; 
				 ?>
				
			<?php endif;
        ?>
<?php
  echo '</div>

</div>
';
?>
<!-- Use any element to open/show the overlay navigation menu -->
            <?php
                if($jumlah > 0){
                    echo '<span id="notification_count" style="float:right">'.$jumlah.'</span>';
                }    
            ?>

            <?php echo'
<span onclick="openNav()" style="float:right;font-size:40px;margin:0px;">=</span>

                ';
            }
            ?>
            <table class="profile_bar">
                <tbody>
                    <tr>
                        <td rowspan="2"><img class="profile_picture" src="guest.png"/></td>
                        <?php
                            echo "<td>Welcome, ";
                            if(isset($_SESSION['NIM'])){
                                echo '<a href="profile.php?nim=';
                                echo $_SESSION['NIM'];
                                echo '">';
                                echo $_SESSION['FIRST_NAME'];
                                echo '</a>';
                                echo'
                                <tr>
                                    <td><a href="logout.php">Logout</a></td>
                                </tr>';
                            }
                            else{
                                echo "Guest";
                                 echo'
                                <tr>
                                    <td><a href="login.php">Login</a></td>
                                </tr>';
                            }
                                
                            echo "</td>";
                        ?>
                    </tr>
                    
                    
                </tbody>
            </table>
        </div>
        <div id="containernav">
            <ul>
                <li><a href="home.php">HOME</a></li>
                <li><div class="dropdown">
                    <a class="active" href="#" class="dropbutton">PORTFOLIO</a>
                    <div class="dropdown-content">
                        <a href="category.php?type=websites">WEBSITES</a>
                        <a href="category.php?type=applications">APPLICATIONS</a>
                        <a href="category.php?type=multimedia">MULTIMEDIA</a>
                    </div>
                </div>
                </li>
                <li><a href="about.php">ABOUT</a></li>
                <li class="right">
                    <form id="search_bar" method="GET" action="search.php">
                        <input type="search" id="input_text" name="key" placeholder="Search">
                        <select name = "type">
                            <option value="all">All</option>
                            <option value="users">Users</option>
                            <option value="posts">Posts</option>
                        </select>
                        <button><img class="search_logo" src="search-256.png"></button>
                    </form>
                </li>
            </ul>
        </div>
        <div id="containersection">
            <h1 class="most_liked"><?php echo $type;?></h1>
            <?php
            if(isset($_GET['sort'])){
                if($_GET['sort'] == 'favorite'){
                    echo '
            <a href="category.php?type='.$type.'&sort=favorite"><div class="sort_by_container"><div class="sort_by">
                <div class="sort_button active">
                    <img class="sort_logo_size" src="hot.png"/>
                    <p class="sort_desc">Most Favorited</p>
                </div>
            </div></a>
            <a href="category.php?type='.$type.'&sort=comment"><div class="sort_by">
                <div class="sort_button">
                    <img class="sort_logo_size" src="favourite.png"/>
                    <p class="sort_desc">Most Commented</p>
                </div>
            </div></a>
            <a href="category.php?type='.$type.'&sort=new"><div class="sort_by">
                <div class="sort_button">
                    <img class="sort_logo_size" src="recent.png"/>
                    <p class="sort_desc">Newest</p>
                </div>
            </div>
            </div></a>';
                }
                else if($_GET['sort'] == 'comment'){
                    echo '
            <a href="category.php?type='.$type.'&sort=favorite"><div class="sort_by_container"><div class="sort_by">
                <div class="sort_button">
                    <img class="sort_logo_size" src="hot.png"/>
                    <p class="sort_desc">Most Favorited</p>
                </div>
            </div></a>
            <a href="category.php?type='.$type.'&sort=comment"><div class="sort_by">
                <div class="sort_button active">
                    <img class="sort_logo_size" src="favourite.png"/>
                    <p class="sort_desc">Most Commented</p>
                </div>
            </div></a>
            <a href="category.php?type='.$type.'&sort=new"><div class="sort_by">
                <div class="sort_button">
                    <img class="sort_logo_size" src="recent.png"/>
                    <p class="sort_desc">Newest</p>
                </div>
            </div>
            </div></a>';
                }
                else if($_GET['sort'] == 'new'){
                    echo '
            <a href="category.php?type='.$type.'&sort=favorite"><div class="sort_by_container"><div class="sort_by">
                <div class="sort_button">
                    <img class="sort_logo_size" src="hot.png"/>
                    <p class="sort_desc">Most Favorited</p>
                </div>
            </div></a>
            <a href="category.php?type='.$type.'&sort=comment"><div class="sort_by">
                <div class="sort_button">
                    <img class="sort_logo_size" src="favourite.png"/>
                    <p class="sort_desc">Most Commented</p>
                </div>
            </div></a>
            <a href="category.php?type='.$type.'&sort=new"><div class="sort_by">
                <div class="sort_button active">
                    <img class="sort_logo_size" src="recent.png"/>
                    <p class="sort_desc">Newest</p>
                </div>
            </div>
            </div></a>';
                }
                
            }
            else{
                echo '
            <a href="category.php?type='.$type.'&sort=favorite"><div class="sort_by_container"><div class="sort_by">
                <div class="sort_button">
                    <img class="sort_logo_size" src="hot.png"/>
                    <p class="sort_desc">Most Favorited</p>
                </div>
            </div></a>
            <a href="category.php?type='.$type.'&sort=comment"><div class="sort_by">
                <div class="sort_button">
                    <img class="sort_logo_size" src="favourite.png"/>
                    <p class="sort_desc">Most Commented</p>
                </div>
            </div></a>
            <a href="category.php?type='.$type.'&sort=new"><div class="sort_by">
                <div class="sort_button active">
                    <img class="sort_logo_size" src="recent.png"/>
                    <p class="sort_desc">Newest</p>
                </div>
            </div>
            </div></a>';
            }
            
            ?>
            <br/>
            
            <?php
            require_once("database.php");
            $post_array;
            if(isset($_GET['sort'])){
                if($_GET['sort'] == 'favorite'){
                    $post_array = get_category_posts_favorite($type);
                }
                else if($_GET['sort'] == 'comment'){
                    $post_array = get_category_posts_comment($type);
                }
                else if($_GET['sort'] == 'new'){
                    $post_array = get_category_posts($type);
                }
            }
            else{
                $post_array = get_category_posts($type);
            }
        $post_array = get_category_posts($type);
        if(count($post_array) > 0):
				foreach($post_array as $row): ?>
				
					<?php
                    echo'
                        <div class="item2">
                <img class="item2_photo" src="'.$row['location'].'"/> 
                <div class="item2_description_layer">
                    <h3 class="item2_description">'.$row['title'].'</h3>;
                    <p class="item2_description">'.$row['desc'].'<a class="link" href="post.php?id='.$row['id'].'">See More..</a></p>
                </div> </div>
            
                    ';
                    ?>
        
			<?php endforeach; 
				else: ?>
				<h3>No posts available/found!</h3>
			<?php endif;
        ?>
            
            
            
        </div>
        <div id="containerfooter">
            <hr>
            <table>
              <tr>
                <td><p id="footerkiri"><i>&copy; 2016 | UKDW Creative</i></p></td>
                <td><p id="footerkanan"><i>Progweb Ceria's Team</i></p></td>
              </tr>
            </table>
        </div>
    </body>
</html>