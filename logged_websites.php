<!DOCTYPE html>

<html>
    <head>
        <title>UKDW Creative - Websites</title>
        <link type="text/css" rel="stylesheet" href="design.css">
    </head>
    
    <body>
        <div id="containerheader">
            
            <a href="logged_home.php"><img id="leftlogo" src="uc.png"/></a>
            <a href="plus.google.com"><img id="gplus" src="gplus-warna.png"/></a>
            <a href="twitter.com"><img id="twitter" src="twitter-warna.png"/></a>
            <a href="facebook.com"><img id="fb" src="fb-warna.png"/></a>
            
            <table class="profile_bar">
                <tbody>
                    <tr>
                        <td rowspan="2"><img class="profile_picture" src="guest.png"/></td>
                        <td>Welcome,Anton</td>
                    </tr>
                    <tr>
                        <td><a href="logout.php">Logout</a></td>
                    </tr>
                    
                </tbody>
            </table>
        </div>
        <div id="containernav">
            <ul>
                <li><a href="logged_home.php">HOME</a></li>
                <li><div class="dropdown">
                    <a class="active" href="#" class="dropbutton">PORTFOLIO</a>
                    <div class="dropdown-content">
                        <a href="logged_websites.php">WEBSITES</a>
                        <a href="logged_applications.php">APPLICATIONS</a>
                        <a href="logged_multimedia.php">MULTIMEDIA</a>
                    </div>
                </div>
                </li>
                <li><a href="logged_about.php">ABOUT</a></li>
                <li class="right">
                    <div id="search_bar">
                        <input type="search" id="input_text" name="googlesearch" placeholder="Search">
                        <button><img class="search_logo" src="search-256.png"></button>
                    </div>
                </li>
            </ul>
        </div>
        <div id="containersection">
            <h1 class="most_liked">Website</h1>
            <div class="sort_by_container">
            <div class="sort_by">
                <div class="sort_button active">
                    <img class="sort_logo_size" src="hot.png"/>
                    <p class="sort_desc">Most Favorited</p>
                </div>
            </div>
            <div class="sort_by">
                <div class="sort_button">
                    <img class="sort_logo_size" src="favourite.png"/>
                    <p class="sort_desc">Most Commented</p>
                </div>
            </div>
            <div class="sort_by">
                <div class="sort_button">
                    <img class="sort_logo_size" src="recent.png"/>
                    <p class="sort_desc">Newest</p>
                </div>
            </div>
            </div>
            <br/>
            <div class="item2">
            <img class=item2_photo src="characters-red.png"/> 
            
            <div class="item2_description_layer">
                <h3 class="item2_description">Angry Bird</h3>
                <p class="item2_description">Angry Birds adalah permainan video yang pada awalnya tersedia untuk aplikasi iPad dan iPhone, namun kini telah dirilis di berbagai media. <a class="link" href="logged_angry_birds.php">See More..</a></p>
            </div>
        </div>
            <div class="item2">
            <img class=item2_photo src="zenonia-5-banner.jpg"/> 
            
            <div class="item2_description_layer">
                <h3 class="item2_description">Zenonia 5</h3>
            </div>
        </div>
            <div class="item2">
            <img class=item2_photo src="iron_man_and_war_machine.0.jpg"/> 
            
            <div class="item2_description_layer">
                <h3 class="item2_description">Iron Man</h3>
            </div>
        </div>
            <div class="item2">
            <img class=item2_photo src="ASIMO_Conducting_Pose_on_4.14.2008.jpg"/> 
            
            <div class="item2_description_layer">
                <h3 class="item2_description">Asimo</h3>
            </div>
        </div>
            <div class="item2">
            <img class=item2_photo src="ASIMO_Conducting_Pose_on_4.14.2008.jpg"/> 
            
            <div class="item2_description_layer">
                <h3 class="item2_description">Asimo</h3>
            </div>
        </div>
            <div class="item2">
            <img class=item2_photo src="ASIMO_Conducting_Pose_on_4.14.2008.jpg"/> 
            
            <div class="item2_description_layer">
                <h3 class="item2_description">See Another Post</h3>
                <img class="next" src="right_arrow.png"/>
            </div>
        </div>
            <div class="spacing_bawah">
            </div>
        </div>
        <div id="containerfooter">
            <hr>
            <table>
              <tr>
                <td><p id="footerkiri"><i>&copy 2016 | UKDW Creative</i></p></td>
                <td><p id="footerkanan"><i>Progweb Ceria's Team</i></p></td>
              </tr>
            </table>
        </div>
    </body>
</html>