<!DOCTYPE html>
<html>
    <head>
        <title>Welcome to UKDW Creative</title>
        <meta charset="UTF-8">
        <link type="text/css" rel="stylesheet" href="design.css">
    </head>
    
    <body>
        <img class="spacing" src="uc.png" alt="Logo"/>
        <div class="isi">
            <div class="middle">
                <div class ="logo">
                    <img class="deco1" src="gunungan.png" alt="Gunungan"/>
                    <img class="deco2" src="kuas.png" alt="Kuas"/>
                    <img class="deco3" src="palet.png" alt="Palet"/>
                    <img class="deco4" src="mouse.png" alt="Mouse"/>

                </div>
                
                <div class="customer">
                    <div class="space_dalam"></div>
                    <a href="home.php"><div class="as_guest">
                        <img class="logo_image" src="guest.png" alt="ProfilePicture"/>
                        <p>Masuk Sebagai Tamu</p>
                    </div></a>
                    <br/>
                    <br/>
                    <a href="login.php"><div class="as_guest">
                        <img class="logo_image" src="guest.png" alt="ProfilePicture"/>
                        <p>Masuk Sebagai Mahasiswa UKDW</p>
                    </div></a>
                </div>
            </div>
        </div>
    </body>
</html>