<!DOCTYPE html>

<html>
    <head>
        <title>UKDW Creative - Home</title>
        <link type="text/css" rel="stylesheet" href="design.css">
    </head>
    
    <body>
        <div id="containerheader">
            <a href="logged_home.php"><img id="leftlogo" src="uc.png"/></a>
            <a href="plus.google.com"><img id="gplus" src="gplus-warna.png"/></a>
            <a href="twitter.com"><img id="twitter" src="twitter-warna.png"/></a>
            <a href="facebook.com"><img id="fb" src="fb-warna.png"/></a>
            <table class="profile_bar">
                <tbody>
                    <tr>
                        <td rowspan="2"><img class="profile_picture" src="guest.png"/></td>
                        <td>Welcome,Anton</td>
                    </tr>
                    <tr>
                        <td><a href="logout.php">Logout</a></td>
                    </tr>
                    
                </tbody>
            </table>
        </div>
        <div id="containernav">
            <ul>
                <li><a class="active" href="logged_home.php">HOME</a></li>
                <li><div class="dropdown">
                    <a href="#" class="dropbutton">PORTFOLIO</a>
                    <div class="dropdown-content">
                        <a href="logged_websites.php">WEBSITES</a>
                        <a href="logged_applications.php">APPLICATIONS</a>
                        <a href="logged_multimedia.php">MULTIMEDIA</a>
                    </div>
                </div>
                </li>
                <li><a href="logged_about.php">ABOUT</a></li>
                <li class="right">
                    <div id="search_bar">
                        <input type="search" id="input_text" name="googlesearch" placeholder="Search">
                        <button><img class="search_logo" src="search-256.png"></button>
                    </div>
                </li>
            </ul>
        </div>
        <div id="containersection">
            <h1>Angry Birds</h1>
            <p class="title">Category : <a href="logged_multimedia.php">Multimedia</a></p>
            <p class="maker">Posted by : <a href="logged_profile.php">Anton Susilo</a></p>
            <a href="plus.google.com"><img id="gplus" src="gplus-warna.png" title="Anton Susilo"/></a>
            <a href="twitter.com"><img id="twitter" src="twitter-warna.png" title="Follow @LJ_Liong"/></a>
            <a href="facebook.com"><img id="fb" src="fb-warna.png" title="Anton Susilo"/></a>
            <br>
            <br>
            <br>
            <hr/>
            <div class="item2_detail_profile">
                <img class="image_detail" src="characters-red.png">
                <p>Angry Birds adalah permainan video yang pada awalnya tersedia untuk aplikasi iPad dan iPhone, namun kini telah dirilis di berbagai media. Permainan ini membuat ketagihan penggunanya sehingga telah diunduh lebih dari 1 miliar pengguna. Bahkan pejabat Inggris pun tertarik dengan permainan ini.</p>
            </div>
            <div class="interaction_tab">
                <div class="comments">
                    <p class="count">1</p>
                    <img class="interaction" src="comment.png"/>
                </div>
                
                <div class="comments">
                    <p class="count">30</p>
                    <img class="interaction" src="favourite.png"/>
                </div>

                <div class="comments">
                    <p class="count">1.2K</p>
                    <img class="interaction" src="mata.png"/>
                </div>
                
            </div> 
        </div>
        <div id="containerfooter">
            <hr>
            <table>
              <tr>
                <td><p id="footerkiri"><i>&copy 2016 | UKDW Creative</i></p></td>
                <td><p id="footerkanan"><i>Progweb Ceria's Team</i></p></td>
              </tr>
            </table>
        </div>
    </body>
</html>